:PROPERTIES:
:ID:       f8475fe3-78a2-46dd-980d-7b4b3edcb528
:END:
#+TITLE: Cell Wall Architecture and Dynamics
#+DATE: 2021-09-30 (10:51)
#+ROAM_REF: [[~/Documents/University/Y4S1/Microbial Structure and Dynamics/Cell Wall Architecture and Dynamics.pdf]]

Tags: [[id:9cd2aa9d-f592-49ce-ba43-0a3765723cff][Microbial Structure and Dynamics]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][Biology]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][University]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][Scratch]]

* Overview
** Gram-Positive
[[file:.orgimg/Structure/2021-12-28_11-11-52_screenshot.png]]
** Gram-Negative
[[file:.orgimg/Structure/2021-12-28_11-12-27_screenshot.png]]
* Cell Wall Components
** [[id:59a08af0-48e4-44f4-9c28-98e3b3159320][Peptidoglycan]]
   - Peptidoglycan is responsible for holding the bacterial cells together – it
     resists the high turgor pressure of the cell caused by the high
     concentration of solutes in the cytoplasm (relative to their outside
     environment)
   - The chemistry of the wall is mostly conserved and shared between
     bacteria. With that said, the shape of different bacteria varies wildly, so
     the structure, the architecture of the PG varies significantly
   - Glycan strand length can vary between species and isn't actually correlated
     to wall thickness! /S. aureus/ has short glycans (18 sugars) and
     /B. subtilis/ quite long ones (50-150 units)
   - Longer strands make for generally stiffer (less stretchy) peptidoglycan
*** Superstructure
    - Concentric rings are seen on newly exposed septal material
    - Coarse mesh on the outside of old material or in areas where septums don't
      form
    - Fine mesh on the inside of the wall with randomly oriented strands in
      /S. aureus/ and concentric strands in /B. subtilis/
    - Cytoplasm side of the septum is a fine mesh like the rest of the inside of
      the cell
    - The centre of the septum (the division plane) looks like the newly exposed
      septal material on the outside of the cell (concentric rings)
**** /E. faecalis/
     - AtlA is a hydrolase that uses LysM domains to specifically target and
       cleave the bacterial septum and not the rest of the cell
     - They likely do this by targeting denuded glycan strands (those lacking
       peptides)
**** /S. aureus/
     - Live /S. aureus/ was stuck into some holes in the Si mesh
     - Newly exposed PG material has a ring-like structure, while older material
       has a more mesh-like structure (it's more knobbly)
     - Glycan strands (in the ring structures) are about 2.5 nanometres apart,
       confirmed with AMF
     - The older material with a very spongy, mesh-like structure containing a
       large number of holes in the surface
     - The cell wall is almost 30nm deep, and the holes are measured as at least
       23nm deep (though they likely go all the way through the wall, but the AMF
       tip can't reach that far)
     - PG is the framework for scaffolding / facilitating:
       - Wall teichoic acid
       - Proteins
       - Secretion
       - Defence
       - Phage binding
     - Phage receptors for /S. aureus/ are wall teichoic acids, which often
       decorate holes on the surface of the PG
     - These holes likely attract the phage with WTA, then they can reach the
       membrane though the hole – the WTA are a way of hiding from the immune
       system
     - If the PG sacculus is purified, the inside of the PG can be examined
     - The inside and the outside look quite different. The inside looks like a
       much finer mesh, something more akin to a sive
     - The inside is where the new PG is being made, so that's why the holes are
       finer! It's being laid down there before being remodelled
     - The sacculi are a bit like a hydrogel – the structure expands and becomes
       thicker after absorbing water (a bit like rehydrating a dry sponge)
***** Diagrams
****** Dividing Cells
 [[file:.orgimg/Cell_Wall_Components/2021-12-28_12-06-41_screenshot.png]]
****** Newly Exposed Rings
 [[file:.orgimg/Cell_Wall_Components/2021-12-28_12-10-02_screenshot.png]]
****** Matured Mesh
 [[file:.orgimg/Cell_Wall_Components/2021-12-28_12-10-49_screenshot.png]]
****** Topographical Map
 [[file:.orgimg/Cell_Wall_Components/2021-12-28_12-45-33_screenshot.png]]
****** Inside of the Sacculus
 [[file:.orgimg/Cell_Wall_Components/2021-12-28_13-08-53_screenshot.png]]
****** Hydrogel
 [[file:.orgimg/Cell_Wall_Components/2021-12-28_13-12-30_screenshot.png]]
**** /B. subtilis/
     - Does B subtilis look similar? From the outside, it seems so! It also has
       a number of mesh-like holes
     - There are rings of newer septal growth at either end, with one pole
       always older than the other
     - Similar coarse-fine mesh from outside to inside
     - Also has rings around the short axis that allows it to keep its rod shape
     - These rings aren't helically wound cables, but are actually bundles of
       20-30 parallel glycan strands ~80nm across. These non-wound bundles form
       a series of furrows in the cell wall
     - The internal, rod-part of the peptidoglycan contains dense, concentric
       rings of PG
     - In the dispersive growth of B subtilis, new rings are introduced below
       and between these concentric rings, allowing for elongation
     - The poles don't turn over very quickly, therefore the ring-like exposed
       septum structure sticks around for quite a while
***** Three-for-One Diagram
      - Circles are strands of glycan
      - Links are cross-links between chains
[[file:.orgimg/Cell_Wall_Components/2021-12-28_14-32-01_1-s2.0-S0300908400012268-fx1.jpg]]
**** /E. coli/
    - Can't look at the live cells because of the outer membrane – sacculi need
      to be purified
    - Poly-L-ornithine was used to adhere the sacculi to the surface, fixing it
      for AFM
    - The very thin PG (mostly a single layer) has:
      - Pores
      - Overlapping layers (which are higher)
      - Some very long glycans where found oriented circumferentiallly around
        the cell
      - When a spherical /E. coli/ is induced, genetically or chemically, the
        long circumfrential strands are replaced with short, disordered strands
        (as in /S. aureus/)
    - Some other gram-negatives (like /Deinococcus/) have a rather thick PG
      layer between its two membranes
    - The long strands were purified by removing the side-chain and doing a gel
      filtration – we could biochemically confirm the existence of the long
      glycans seen in AFM
***** Diagram
[[file:.orgimg/Cell_Wall_Components/2021-12-28_14-40-39_screenshot.png]]
**** Summary Diagram
[[file:.orgimg/Cell_Wall_Components/2021-12-28_14-22-44_screenshot.png]]
*** Dynamics (/S. aureus/)
**** Division
     - S aureus divides in 3 planes (the orthogonal 3D planes)
     - After division, the daughter cells end up with a pie-crust (remnants of
       septation from previous divisions)
     - A spherical cell can be divided into 8^ths with pie-crust ribs separating
       each section
     - These structures are inherited from generation to generation and may
       inform the cell as to which plane should be divided on next (by creating
       a unique structure at the intersection of ribs)
     - Some parts of the cell wall are much older than other parts of it
       (potentially from many generations back!)
     - This cell sectoring leads to a heterogeneity in the population, with some
       cells having older sections of peptidoglycan and others having newer
       regions. This diversity allows the population to better survive changing
       conditions that may favour one sort of wall over another
***** Diagram
[[file:.orgimg/Cell_Wall_Components/2021-12-29_15-20-32_screenshot.png]]
**** Growth
     - By consecutively labelling D-amino acids (usually alanine) in different
       colours (say, a new colour every 5 minutes), you can see the order than
       peptidoglycan in synthesised in
     - Material seeems to be constantly added all around the cell in random
       places, but the septum is built in a very structured order (outside-in)!
     - These septums can be fluorescently labelled and the inherited structures
       can be seen along the 3 division planes
     - Dynamics of growth involve a balance of synthesis and also some hydrolysis
     - Old material needs to be cut to allow for the new material to take up the
       strain of the turgor pressure
***** Diagrams
[[file:.orgimg/Cell_Wall_Components/2021-12-29_16-06-49_screenshot.png]]
[[file:.orgimg/Cell_Wall_Components/2021-12-29_16-07-08_screenshot.png]]
**** Septation
     - STORM, individual labelled D-ala nodes can be visualised
     - Down at 15 seconds resolution, it seems that mostly all new synthesis is
       all over the place. There isn't really a foci of division (no strong ring
       representing a leading-edge of synthesis)
     - Why don't /S aureus/ make their septum from the outside in always? The
       septum is V-shaped, with a point at the leading edge
     - This V-shape of growth makes it easy to add new material on a much larger
       surface area. If the septum had parallel sides, then you could only
       deposit new material in the small inside ring
     - Because PG is being added all along the face of the wedge, synthesis
       appears spread across the whole septum
***** Diagrams
[[file:.orgimg/Cell_Wall_Components/2021-12-29_16-23-19_screenshot.png]]
[[file:.orgimg/Cell_Wall_Components/2021-12-29_16-38-22_screenshot.png]]
[[file:.orgimg/Cell_Wall_Components/2021-12-29_16-44-00_screenshot.png]]
[[file:.orgimg/Cell_Wall_Components/2021-12-29_16-46-41_screenshot.png]]
*** Function (in /S. aureus/)
**** Division
     - S. aureus division starts with septum formation (a pie-crust-like ring),
       then things divide into graphfruit-like halves before rounding out again
***** Diagram
 [[file:.orgimg/Cell_Wall_Components/2021-12-28_11-21-11_screenshot.png]]
**** Cell Death
     - Studying the cell wall is important, as it has vital medical
       implications. A number of antibiotics targets the production of cell wall
       to kill off bacteria. Penicillin and vancomycin are examples
     - This cell lysis is likely the result of ongoing peptidoglycan hydrolysis
       without the balancing amount of PG synthesis (as that's inhibited by the
       antibiotic)
     - The hydrolysis and weakening of the PG causes the cells to swell from the
       osmotic pressure and eventually lyse
* Imaging the Cell Wall
  - How can you work out the architecture of the cell wall if you're only
    looking at the parts? Up until around a decade ago we didn't have any direct
    evidence of the PG structure
  - To acquire this direct evidence, we need to examine the sacculus in-situ,
    but how do you examine it without breaking up the peptidoglycan molecule?
** [[id:ba8868cd-cbb2-4023-8037-9aff89db9f23][Atomic Force Microscopy]] (AFM)
   - Atomic Force Microscopy was / is the key to examining the in-situ structure
     of cell walls (resolution of less than 1 nm)
   - The AFM samples need to be fixed in place! On a flat surface, they could
     roll / be dragged around, so a silicon mesh was made that allows for the
     embedding of cells into a sort of base-plate – fixing them in a location
   - A slicing technique of the AMF image was used to build a 3D model of the
     holes (almost cave-like) in the cell surface
** Super Resolution Microscopy
   - These techniques allow fluorescent microscopy at a higher resolution than
     traditional light microscopy
*** Structured Illumination Microscopy (SIM)
    - Resolution limit of 120nm
    - Relies on moire interference patterns generated by moving a fine grating
      over the sample
    - A mathematical reconstruction allows for the creation of a high resolution
      image
**** Diagram
[[file:.orgimg/Imaging_the_Cell_Wall/2021-12-29_15-31-57_screenshot.png]]
*** Stochastic Optical Reconstruction Microscopy (STORM)
    - Resolution limit of 25nm
    - Uses blinking dyes (a bit hard to come across) and reconstructs points of
      light from Gaussian distributions
    - After >1,000 images capturing different blinks, a full image can be
      reconstructed
**** Diagram
[[file:.orgimg/Imaging_the_Cell_Wall/2021-12-29_15-32-53_screenshot.png]]
* Scratch
  - Scaffold Model
    - Glycan strands run perpendicular to the membrane
    - This doesn't seem to be a very realistic model (certainly not in
      gram-negatives)
  - Layered / Planar Model
    - Glycan strands run parallel to the membrane – stacked on top of each other
  - STORM relies on fluorescent dyes that blink, instead of being continuously
    illuminated
