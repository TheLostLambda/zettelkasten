:PROPERTIES:
:ID:       905052b9-949d-4fa3-a474-01de79b75f15
:END:
#+TITLE: Immunology
#+DATE: 2021-05-20 (20:56)
#+ROAM_REF: [[file:~/Documents/University/Y3S2/Microbiology/MBB263 Immunology 1a Innate Immunity (LJP).pdf]]

Tags: [[id:3c85cd21-6e45-49f1-bf55-fbfa2b153c2f][§Microbiology 2]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]]

* Overview
  - The immune system is a set of cells and molecules that defends the body
    against disease
  - Divided into [[id:782c0fd8-c741-4d49-8701-3a7b1c73efb2][§Innate Immunity]] and [[id:f8922f3a-5df9-4ef3-8b75-54ba73b0345a][§Adaptive Immunity]]
