:PROPERTIES:
:ID:       78ff19c3-f717-4b5d-9f48-0597eb3500e7
:END:
#+TITLE: Pathogenesis
#+DATE: 2021-05-18 (20:45)
#+ROAM_REF: [[file:~/Documents/University/Y3S2/Microbiology/MBB263_wk6_videos1-3_slides.pdf]]

Tags: [[id:3c85cd21-6e45-49f1-bf55-fbfa2b153c2f][§Microbiology 2]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]] [[id:c36c7805-c91c-4e04-b522-aaa940f67ff9][§Disease]]

* Overview
  - Multicellular organisms on Earth are outnumbered trillions to one by
    unicellular microbes
  - Animals are essentially hybrid organisms with a more or less equal number of
    eukaryotic and prokaryotic cells
  - The vast majority of these passenger organisms are entirely harmless or even
    actively beneficial – less than 100 species of bacteria are responsible for
    all known bacterial infectious disease
  - Disease can be caused by bacterial of infection of pretty much any body site
    and symptoms vary significantly depending on the site of damage and the
    response of the immune system
  - We study pathogenesis to guide the design of pathogen-specific therapeutics
    (drugs that target only disease-causing organisms and not commensals)
** Disease Progression Diagram
[[file:.orgimg/Overview/2021-05-18_21-27-42_screenshot.png]]
* Terminology
  - Pathogen :: an organism that, by it' actions, causes harm to its host
  - Commensal :: a commensal organism benefits from the interaction with the host
    and the host neither benefits nor is harmed
  - Opportunistic Pathogen :: an organism that can cause disease in its host
    given the right set of circumstance but is otherwise a commensal
  - Zoonotic Pathogen :: an organism with an animal reservoir (either as a
    pathogen or commensal) that can be transmitted to and cause disease in
    humans
  - Pathogenesis :: the mechanism of disease, the continued process maintaining
    a diseased state
  - Pathogenicity :: ability of an organism to cause disease
  - [[id:6a843060-2191-4223-b7f9-77531092e60f][§Virulence]] :: degree / severity of pathogenicity
* Koch's Postulates
  1) The microorganism must be found in abundance in all organisms suffering
     from the disease but not in healthy organisms
  2) The microorganism must be isolated from a diseased organism and grown in
     pure culture
  3) The cultured microorganism should cause disease when introduced into a
     healthy organism
  4) The microorganism must be re-isolated from the diseased experimental host
     and identified as being identical to the original causative agent
** Molecular Postulates (For Identifying Virulence Factors)
   1) The phenotype / property / gene under investigation should be associated
      with pathogenic members of a genus or pathogenic strains of a species
   2) Specific inactivation of the gene(s) associated with the suspected
      virulence trait should lead to a measurable loss in pathogenicity or
      virulence
   3) Reversion or allelic replacement of the mutated gene should lead to
      restoration of pathogenicity
** Pathogens that Violate the Postulates
   - When a pathogenic organism secretes a toxin, the organism itself doesn't
     actually need to be present to cause disease (only the toxin is needed)
   - Many organisms can be grown in pure culture, usually because of complex or
     completely unknown nutritional requirements (e.g. chlamydia, leprosy,
     syphilis)
   - Growth in the lab can lead to a loss of virulence so the disease cannot be
     recapitulated (e.g. Puumala virus cultured in the lab loses its virulence
     in a matter of hours)
   - Some diseases may be specific to humans or have different effects in
     different organisms, so no suitable model organism is available – this is
     the most common problem when attempting to satisfy Koch's postulates
* Types of Pathogen
** Strict / Professional Pathogens
   - Highly adapted for the pathogenic lifestyle – can't survive outside of the host
   - Often have nutritional requirements that can only be satisfied through
     pathogenesis
   - Often access a unique niche within the host, facing little competition
   - Usually transmit very efficiently between hosts
   - Some are so adapted to the in-host lifestyle that their genome shrinks in
     size – this increases efficiency but means that they can no longer live
     independently
*** Examples
    - Helicobacter pylori :: live in the stomach, cause stomach ulcers and
      cancer, no competitors
    - Neisseria gonorrhoeae :: sexually transmitted infection of the
      genitourinary tract
    - Shingella dysenteriae :: dysentery, gut infection
    - Mycobecterium tuberculosis :: TB, lung (mostly) and some systemic infections
    - Chlamydia trachomatis :: intracellular sexually transmitted infection,
      genitourinary tract, eyes, lungs, etc
** Opportunistic Pathogens
   - Only cause disease when host defences are compromised in some way (e.g. a
     barrier breech like an open wound, an inhibition of clearance like mucus in
     CF lung, a loss of colonisation resistance from antibiotic induced
     dysbiosis, and immunocompromisation from HIV or anti-rejection medications)
   - When not being a pathogen, they live in the environment or exist as commensals
   - Often not efficiently spread between hosts but there are exceptions
*** Examples
    - Pseudomonas aeruginosa :: wound / burn infections, lung infections in
      cystic fibrosis
    - Clostridioides difficile :: inflammatory infection of the large intestine
      but only after antibiotic treatment
    - Staphylococcus epidermidis :: skin and wound infections
    - Staphylococcus aureus :: skin and wound infections, endocarditis,
      osteomyelitis, etc
** Facultative Pathogens
   - Probably the majority of pathogens
   - well-adapted for multiple lifestyles, equally at home in environmental
     niches and when causing disease
   - Flexibility usually means a larger genome
   - Strain variation is important here, as many virulence genes are carried on
     mobile elements. If a strain carries these genes it can be a pathogen, but
     other stains might be harmless
*** Example
    - Escherichia coli :: gut and UTI infections, sometimes leading to sepsis or
      meningitis – sometimes the line between opportunistic and facultative
      pathogens is blurry here
