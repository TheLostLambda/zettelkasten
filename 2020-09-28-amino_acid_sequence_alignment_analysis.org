:PROPERTIES:
:ID:       e1ba8a29-1e7a-4b2a-a467-321bc3240feb
:END:
#+TITLE: Amino Acid Sequence Alignment & Analysis
#+DATE: 2020-09-28 (10:10)
#+ROAM_REF: [[file:~/Documents/University/Y3S1/Practical Molecular Bioscience/MBB265 Protein Structure - Sequence Analysis 2020-21 Part I.pdf]]
#+ROAM_REF: [[file:~/Documents/University/Y3S1/Practical Molecular Bioscience/MBB265 Protein Structure - Sequence Analysis 2020-21 Part II.pdf]]

Tags: [[id:99d03d55-eb13-4207-9887-e5032dfc215b][§Practical Molecular Bioscience 2]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]] [[id:a2aaf63c-66a5-45e7-9725-b0811e902246][§Protein Structure]]
[[id:a12a79a5-4a3b-49ff-8ece-cd4926a7f495][§Bioinformatics]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]]

* ErbB Sequence
P R Y L V I Q G D E R M H L P
** Uniprot Matches
  - =UniProtKB - P00533 (EGFR_HUMAN)=
  - =UniProtKB - A0A1D5PKH0 (A0A1D5PKH0_CHICK)=
  - =UniProtKB - G1NIB9 (G1NIB9_MELGA)=
** LALIGN Dotplot
[[file:.orgimg/ErbB_Sequence/2020-09-28_11-27-49_screenshot.png]]
** Analysis
  - Matches point to an epidermal growth factor receptor (EGFR)
  - While this protein is of viral origin, its similarity to a growth-factor
    receptor may explain its oncogenic activity
  - Belongs to the receptor protein-tyrosine kinase family
  - The diagonal line in the dotplot shows that ErbB (the y-axis) matches with
    the ~590 to ~1170 region of the EGFR (the x-axis)
  - The displacements are the product of a frame shift following, in the first
    case, a one residue deletion.
*** Evolutionary Roots
  - The fact that ErbB shares up to 93% of its residues with EGFR seems to imply
    more than convergent evolution or coincidence. Genes with as low as 30%
    similarity can share a function. *That seems to imply that this ErbB gene
    was scavenged from a host cell during viral replication*.
*** False Hits
  - If you pick a region of the protein that contains a common motif region (say
    one the spans a membrane, or binds a common cofactor like ATP), then you
    might return a massive class of proteins that might not actually share much
    primary activity with the sequence you set out to investigate.
  - Once you find a hit by searching 15 residues or so, your could improve your
    confidence by comparing either larger regions of the protein or by drawing
    on your knowledge of the function of both sequences and ascertaining
    whether there is a tangible link.
* Sequence Alignment
** Digital Tools
*** Protein BLAST
  - http://www.uniprot.org/blast/
  - http://blast.ncbi.nlm.nih.gov/Blast.cgi
*** Alternatives
  - DIAGON and LALIGN produce "dotplots"
  - https://www.ebi.ac.uk/Tools/psa/lalign/
** Mismatches in pairings
  1) WHATISTHIS and WHYISSTHIS
     - 3
  2) WHATISTHIS and WEATESTHIS
     - 2
     - Changing a hydrophobic I for a charged E is likely to disrupt the
       properties of the chain
  3) WHATISTHIS and WRATISTHIN
     - 2
     - Substituted AAs are in the same class, so it's not a massive difference
     - This is likely to give the best match
** DIAGON Alignment
*** Single Match Matrix
[[file:.orgimg/Sequence_Alignment/2020-09-28_11-06-04_DIAGON.png]]
**** Analysis
  - The diagonal line shows where the two sequences are aligned with each other
    - =   ARCACF   =
    - =ACDARCACFRRA=
  - Sometimes is can be difficult to pick out the diagonal line from the many
    X's. We can decrease the sensitivity of the test by requiring that entire
    triplets of sequence match
*** Triplet Match Matrix
[[file:.orgimg/Sequence_Alignment/2020-09-28_11-17-28_DIAGON3.png]]
**** Analysis
  - This is using a 'comparison length' or 'window' of 3 residues
  - In practice, for very long protein sequences, a window of 15 or more
    residues may be used
  - Computer programs also use a percent similarity and don't need identical
    matches. Substituting an AA with similar properties can still be counted as
    a match. This "threshold" is set by the user.
*** Computer Generated Dotplot
[[file:.orgimg/Sequence_Alignment/2020-09-28_11-23-22_screenshot.png]]
**** Analysis
  - Aligning a protein with itself can reveal repeated regions
* Self Alignment
  - Dotplots of a protein and itself can reveal repeated regions of the protein
    structure.
** Dotplot
[[file:.orgimg/Self_Alignment/2020-09-28_11-44-43_screenshot.png]]
*** Analysis
  - This dotplot has a solid diagonal line like all self-alignments should
    (they are the same sequence, after all), but also shows some off-axis
    diagonals that are repeated motifs.
  - The region starting at AA 20 is repeated in regions 120 and 220
** Challenge
[[file:.orgimg/Self_Alignment/2020-09-28_11-49-39_screenshot.png]]
*** Analysis
  - Many somewhat repeated regions, a possible diagram of motifs is shown below
**** Motif Diagram
[[file:.orgimg/Self_Alignment/2020-09-28_12-08-32_Protein%20Map.png]]
* ErbB Structure
** "Parent" EGFR Structure
  - Extracellular N-terminal domain for binding growth factor
  - Transmembane section with only one pass through the membrane
  - Cytoplasmic C-terminal with tyrosine kinase activity activated by growth
    factor binding
*** Analysis
  - The membrane-spanning amino acids are likely to be hydrophobic (non-polar)
    in nature so that they can easily pass through the hydrophobic tails of the
    membrane phospholipids.
  - The most common amino acids for this would be the 9 non-polar amino acids:
    Glycine, Alanine, Proline, Valine, Leucine, Isoleucine, Methionine,
    Tryptophan, and Phenylalanine.
*** Hydropathy Plot
[[file:.orgimg/ErbB_Structure/2020-09-29_10-26-37_screenshot.png]]
**** Analysis
  - The transmembrane region seems to be from 649-668, as that's a stretch of
    hydrophobic residues with some polar ones on either side.
    - 640 PKIPSIAT|GM VGALLLLLVV ALGIGLFM|RR
    - The region is 20 AA long.
  - A membrane is usually 35Å across, so we are looking for some structure that
    gets us about there in 20 residues.
    - The rise per residue in an alpha helix is ~1.5Å and ~3.5Å in beta-sheets.
    - It's more likely that this transmembrane region is an alpha helix, as 20 \times
      1.5 is 30Å. This spans most of the hydrophobic portion of the membrane and
      allows the polar AAs on either side to interact with the polar head groups.
    - 20 \times 3.5 gets you to 70Å which is a bit too long. Additionally, \beta-strands
      leave the hydrophilic amino-backbone exposed, while \alpha-helices occupy these
      groups with hydrogen bonds so they aren't exposed to the hydrophobic
      inside of the membrane.
*** 3D Structure of Human EGFR
[[file:.orgimg/ErbB_Structure/2020-09-29_11-27-34_screenshot.png]]
**** Analysis
  - The teal protein is the growth factor and the purple and orange proteins are
    two copies of the growth factor receptor.
  - This shows that the growth factor can bind both receptors and helps them
    dimerise – this helps activate the RLK activity of the cytoplasmic domains.
  - The loops between the two extracellular domains appear to be the part
    binding the growth hormone.
  - This is similar to how the hyper-variable loops on antibodies bind
    antigens. These loops bind different sites on the same GF, so they need to
    be specific enough to not falsely trigger, but flexible enough to adapt to
    binding different sites of the GF (as is shown above).
  - While antibodies only interact with one specific feature of an antigen,
    these receptor regions bind different parts of the somatotropin growth
    factor.
  - This structure supports the dimerisation model of activation
    (trans-activation), as two receptors end up bound to the growth factor, not
    just one. The extracellular regions are pulled together, so it's quite
    reasonable that the RLK regions inside the cell would be as well.
** ErbB Structure
*** Hydropathy Plot
[[file:.orgimg/ErbB_Structure/2020-09-29_10-56-45_screenshot.png]]
**** Analysis
  - Transmembrane region is roughly 66-86, but has a lone Cystine in the
    middle. This apparently isn't enough to totally disrupt the structure.
    - 60 SKTPS|IAAGV VGGLLCLVVV GLGIGLYL|RR
  - This membrane-spanning region is the same size as that of the receptor, and
    shares some residues, so it's likely to span the membrane in a similar way.
*** ErbB Diagram
[[file:.orgimg/ErbB_Structure/2020-09-29_11-19-09_ErbB%20diagram.png]]
**** Analysis
     :PROPERTIES:
     :ID:       a0939202-9110-4610-ae01-c1f83f3785a3
     :END:
  - Missing the extracellular growth factor receptor. It's unlikely that the few
    residues on the outside of a cell can still form a functional receptor.
  - The fact that ErbB is an oncogene implies that missing this receptor leaves
    the RTK in an always-on state. It constantly acts as if a GF molecule has
    been bound, even when there is none present. This promotes uncontrolled
    growth.
