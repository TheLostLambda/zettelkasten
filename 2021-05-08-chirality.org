:PROPERTIES:
:ID:       92b1cff7-6a19-4526-89ed-b621dde70ac9
:END:
#+TITLE: Chirality
#+DATE: 2021-05-08 (18:59)
#+ROAM_REF: [[~/Documents/University/Y3S2/Biochemistry/Lectures/lec1-Drugs and Chirality-annotated.pdf]]

Tags: [[id:33ee1804-bcfb-40c5-8f2c-62d54324c757][§Biochemistry 2]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:16c13b20-00de-40eb-94d8-2df33afeb4a3][§Chemistry]]

* Overview
  - Certain enzymes can discriminate between enantiomers which are mirror images
    of each other – no matter how they are rotated, you can't superimpose the
    chiral forms
** Diagrams
[[file:.orgimg/Overview/2021-05-11_13-28-20_screenshot.png]]
* Stereoisomers
  - These are compounds that differ only in their three dimensional arrangement
    of their constituent atoms
  - They can be divided into Enantiomers (non-identical mirror image structures)
    and Diasteroisomers (not mirror images)
  - Enantiomers are all chiral and optically active (rotating polarised light)
  - Diasteroisomers can be further divided into structures that have 2 or more
    chiral centres, or those that exhibit /cis-trans/ isomerism
** Diagram
[[file:.orgimg/Stereoisomers/2021-05-11_14-38-34_screenshot.png]]
* Classification Systems
** L/D
   - Determined by the relative position of substituents to a reference compound
** +/-
   - Rotation of plane polarized light (empirical and enantiomers only)
** R/S
   - Determined via the absolute configuration of atoms around a chiral centre
*** Steps For Assignment
    1) Label the 4 substituent groups by priority
    2) Look from the central atom towards the substituent with the lowest
       priority (the low-priority substituent should be "hidden" behind the
       centre
    3) If the priority of the 3 other groups increases clockwise, it's an R
       conformation, otherwise it's S
**** Priority Rules
     1) Lower *atomic numbers* have a lower priority (e.g. C < O)
     2) Lone pair is a lower priority than H
     3) If there are two substituents with equal rank, proceed along the
        substituent chains from the chiral centre until there is a difference
     4) Double and triple bonds in substituent chains are counted as connecting
        to the other atom twice or thrice (e.g. C-O < C=O)
*** Example Diagram
[[file:.orgimg/Classification_Systems/2021-05-11_15-26-16_screenshot.png]]

