:PROPERTIES:
:ID:       fa51549d-be50-4e08-9023-012ede26a9d6
:END:
#+TITLE: Telomeres
#+DATE: 2020-10-23 (14:54)
#+ROAM_REF: [[~/Documents/University/Y3S1/Genes, Genomes, and Chromosomes/MBB267 genome architecture overview.pdf]]
#+ROAM_REF: [[~/Documents/University/Y3S1/Genes, Genomes, and Chromosomes/Barnes genome architecture slides.pdf]]

Tags: [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:46a40352-2f1a-413b-871a-5ca06ce59a6e][§Genes, Genomes, and Chromosomes]] [[id:7aebf77d-3915-4cca-a5cc-79ec9356b02d][§Non-Coding DNA]]

* Why do we need them?
  - In linear chromosomes, the lagging strand of DNA is slightly truncated
    following each replication
  - This happens because the Okazaki fragments need to be continually primed and
    overwritten with DNA by an upstream polymerase, but there is nothing
    "upstream" of the end of a chromosome
  - It's also important to distinguish between double-stranded DNA breaks that
    need to be repaired, and the natural end of a linear chromosome
** Diagram
[[file:.orgimg/Why_do_we_need_them?/2020-10-23_14-58-16_screenshot.png]]
* What are they?
  - Well-conserved, short, tandem repeats (TTAGGG in all mammals, TTGGGG in
    Tetrahymena, a protozoan model organism with many short chromosomes and
    telomeres)
  - The number of repeats is variable, ~400bp in Saccharomyces, 10-15kb in human
  - There is a 3' overhang, providing a template for the reverse-strand
    synthesis of the end of the chromosome
* How they work
  - A number of specialised protein bind telomere DNA and form protective
    "shelterin" complexes
  - This cap serves to differentiate the end of the chromosome from DNA breaks,
    promotes the formation of a special tertiary structure in the DNA called a
    t-loop (which serves to occupy and protect the 3' overhang), recruit
    telomerase, and protect the end from nucleases
  - They are two-part, with a protein TERT (TElomeric Reverse Transcriptase) and
    an RNA TERC (TElomerase Rna Component)
  - The RNA acts a repetitive template for synthesising new repeats onto the 3'
    overhang of the telomere
  - A DNA polymerase later comes in and grows the 5' strand
** Diagram
[[file:.orgimg/How_they_work/2020-10-23_15-14-00_screenshot.png]]
* Mitotic Clock
  - The number of divisions somatic cells can undergo is often limited by the
    length of their telomeres.
  - In humans, telomerase is only really turned on in stem and germline
    cells. In somatic cells, telemeres are shortened each generation
  - If the telomeres become too short for the "shelterin" complexes to bind,
    then the cell dies or the genome becomes unstable (due to erroneous "repair"
    of the loose chromosome ends)
  - It appears that this process of telomere shortening is associated with aging
    and there is some evidence (from mice) that up-regulating telomerase reduces
    aging
* Cancer
  - While it's unlikely to be the causative mutation, up to 90% of cancers
    contain up-regulated telomerase
  - This allows cancer cells to divide indefinitely without encountering the
    mitotic clock
  - Telomerase inhibitors are therefore a promising drug-target in cancer
