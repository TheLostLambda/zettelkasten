:PROPERTIES:
:ID:       abbd78ca-f699-4ae4-85b5-92d8da8fb114
:END:
#+TITLE: Lewis Structures
#+DATE: 2021-02-13 (10:53)
#+ROAM_REF: [[file:~/Documents/University/Y3S2/Biochemistry/Lectures/lec-1b(1).pdf]]

Tags: [[id:33ee1804-bcfb-40c5-8f2c-62d54324c757][§Biochemistry 2]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:16c13b20-00de-40eb-94d8-2df33afeb4a3][§Chemistry]]

* Overview
  - Lewis structures are a way of showing electrons in a molecular structure
* Drawing Rules / Process
  1) Write the molecular skeleton
  2) Assume all bonds are covalent
  3) Count the available valence electrons
  4) Add sigma bonds and give each atom 8 electrons (2 for H)
  5) If the number of electrons in the structure is the same as the valance
     number from 3, then the structure is correct; otherwise, introduce pi bonds
     until the valence number is matched
** Diagram
[[file:.orgimg/Drawing_Rules_/_Process/2021-02-13_11-08-06_screenshot.png]]
