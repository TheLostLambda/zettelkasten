:PROPERTIES:
:ID:       b53964ae-f123-45ed-8f17-38d96ed5fe74
:END:
#+TITLE: Bacterial Size Matters
#+DATE: 2021-12-21 (21:42)
#+ROAM_REF: [[file:~/Documents/Didactic/Applications/ACCE Doctoral Training Partnership/Bacterial size matters.pdf]]

Tags: [[id:6602882e-b76e-4bdf-a8f5-c1e7dd3ec19d][Science]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][Biology]] [[id:59a08af0-48e4-44f4-9c28-98e3b3159320][Peptidoglycan]]

* Overview
  - Stephané Mesnage sent me this paper to take a look at so I could talk about
    "A recent piece of science" that excited me in my [[id:7a13e58f-8b18-4acf-aa79-046d6008a8bb][ACCE Doctoral Training
    Partnership]] application
  - The headings under ACCE Notes are structured in such a way that each one
    answers one of the posed questions by the prompt
* ACCE Notes
** Outline the project
** Why was it important?
   - /Enterococcus faecalis/ is a major nosocomial (opportunistic) pathogen
     that's intrinsically resistant to a number of antibiotics and can transfer
     resistance to others (new treatments are needed!)
   - ADD SOME DETAIL ON SYMPTOMS / SEVERITY OF INFECTION?
   - The mechanisms underlying its virulence / evasion of the immune system
     remain poorly understood
** What were the hypotheses and why?
   - A distinctive feature of the bacterium is its diplococci / short cell
     chains. This could therefore be an interesting place to start looking for
     virulence factors
   - AtlA contributes to septum cleavage WHY?
*** High AtlA expression doesn't explain dominant role in cleavage
    - Because AtlA is less active than AtlB it needs more expression?
    - Perhaps AtlA only plays a dominant role because there is more of it!
    - Over-expressing AtlB, however, did *not* complement the long cell chains
      of the \Delta{}atlA mutant
*** N-terminal cleavage of AtlA stimulates septum cleavage
    - Previously found that truncation of the N-terminal had a marginal effect
      on AtlA activity when tested against whole sacculi
    - Testing again on a septum of peptidoglycan shows a 10x increase in
      activity! This would drive *cleaved* AtlA to specifically target the
      septum
    - It would also delay septum cleavage until this proteolysis occurs
*** N-terminal AtlA cleavage is required for daughter cell separation
    - Truncation of the N-terminal significantly decreases FSC (and therefore
      cell-chain length)
    - This effect is even more dramatic where some binding LysM domains have
      been removed and overall chain length increased
    - As an additional note, removing two LysM domains dramatically increases
      cell chain length
*** AtlA N-terminal glycosylation inhibits septum cleavage
    - It seems that the N-terminal is O-glycosylated and glycosylated protein
      assays show that the modification is present on the N-terminal (as TEV
      cleavage releases an N-terminal glycoprotein)
    - Removing the /gtfAB/ genes removes this glycosylation
    - Removing this glycosylation leads to *shorter* chains and increased
      activity
*** Swapping catalytic domain doesn't abolish cleavage
    - It was thought that substrate recognition by the catalytic domain was
      essential in AtlA
    - This turned out not to be the case – while the natural glycosidase was the
      most effective, the other peptidoglycan hydrolase domains could still
      cleave the septum efficiently
*** The LysM domains of AtlA are important for efficient cleavage
    - Swapping for AtlB domains drops activity, and adding AtlA LysM domains to
      AtlB takes AtlB from totally ineffective to somewhat comparable with WT
      AtlA
    - The number of LysM repeats in AtlA is also important, with each truncation
      of a LysM domain decreasing activity
*** Longer chain length leads to reduced lethality
    - The longer chains appear to be more readily phagocytosed than the short
      chains / diplococci
    - This resulted in a significantly reduced lethality, but separating long
      cell chains of the \Delta{}AtlA via sonication restored much of this lethality
    - The cell separation is then a sort of virulence factor
** What were the findings and how are they relevant?
   - Found that AtlA is regulated to cleave the septum following cell division
   - This cleavage is a vital component of pathogenicity, as longer chains are
     more susceptible to phagocytosis and can no longer cause lethality in
     zebrafish
   - Inhibition of AtlA could then serve as a potential therapeutic treatment
     for /E. faecalis/ infections
   - This smaller chain -> *more* virulence is somewhat unique. The snake-like
     aggregation of mycobacteria and (single-cell?) filaments of UPEC /E. coli/
     are large structures hard to phagocytose, but here the smaller chains do
     better
   - /S. pneumoniae/ does better with short chains as it avoids opsonisation by
     the complement system, but here the human macrophages were able to
     phagocytose the cell chains without this opsonisation
   - These enterococci (is this referring to the /S. pneumoniae/?) chains can
     form turns and are flexible – making them easier to phagocytose than UPEC
     filaments
   - Dispersed cells can circulate in the zebrafish model
   - Impaired septum cleavage is primarily expected to restrict spreading, not
     phagocytotic clearing
   - Proposed that /E. faecalis/ is prevented from multiplying and releasing the
     metalloprotease GelE when in the phagocytes, preventing tissue damage and
     host death
   - Targeting AtlA activity could prevent virulence while sparing other cells
     in the microbiome from harm
** How would I improve it?
   - Are the cut sites of AtlA* and AtlA_AtlB correct in figure 5? Surely they
     are pointing to the same bond?
   - There is still some work to be done in elucidating how exactly the factors
     controlling AtlA are controlled! What does the proteolysis? How is
     glycosylation regulated?
   - Need to test what about septal peptidoglycan would improve things – try
     some different, synthetic / defined substrates (ask Mesnage about this!)
     Test the denuded hypothesis!
   - They also suggested decoupling the binding and catalytic domains – maybe
     couple this with some fluorescence assay and visualise different
     localisation of the LysM?
** What did I learn about scientific research?
