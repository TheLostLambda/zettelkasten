:PROPERTIES:
:ID:       a2aaf63c-66a5-45e7-9725-b0811e902246
:END:
#+TITLE: Protein Structure and Function
#+DATE: 2020-09-28 (10:19)

Tags: [[id:aca6de40-3d93-439b-aa1a-421b458e28d8][§Biostructures, Energetics, and Synthesis]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]]

* Resources
  - [[file:~/Documents/University/Y3S1/Biostructures, Energetics, and Synthesis/MBB266 Class sheets Chapters 1-3_2020.pdf][Class Sheet]]
* Backbone
  :PROPERTIES:
  :ID:       f966fdb0-7ee8-4f39-9f87-11ad2373adea
  :END:
  - Goes from N to C terminal
  - Conformations are limited by steric clashes between the R-groups.
  - Valid \psi and \phi angle combinations can be visualised on a Ramachandran plot
* Levels of Structure
  - Primary :: AA Sequence of the protein
  - Secondary :: Backbone H-bonding leads to the formation of \alpha-helices and
    \beta-sheets (or things like 3_10 helices)
  - Tertiary :: R-group interactions form remote interactions and build a
    structure out of the secondary motifs. Motifs joined by loops. Cofactors
    belong here?
  - Quaternary :: The assembly of several AA-chains. Can be homo (all the same
    type of chain assembling) or hetero (different types of chain assembling)
    oligomers.
* Molecular Forces
  - Hydrogen bonds :: Often in the backbone, the attraction of Hydrogen dipoles
    to a lone pair of another molecule.
  - Ionic bonds :: Attraction between differently charged residues, strongest in
    the core of a protein, where the dielectric constant is much smaller than in
    water. Not many in proteins.
  - Disulfide bonds :: Covalent bonds between distance cystine hydroxyl
    groups. Only really in extra-cellular proteins and usually only 1 or 2. Very
    strong.
  - Peptide bonds :: Partial double-bond character. Covalent forces binding AAs
    together
  - Van der Waals :: Transient dipoles formed by the random movement of
    electrons can attract long, parallel molecules. Very weak interactions, but
    there are many of them within a protein.
    - Dipole - dipole interactions
    - Dipole - induced dipole interactions
    - London dispersion forces (transitory dipoles)
  - Hydrophobic Interactions :: It's energetically favourable to have
    hydrophobic side-chains near the core of a protein with the hydrophilic ones
    outside. This reduces the number of water molecules that need to arrange
    around hydrophobic surface groups.
** Free Energy
  - $\Delta G = \Delta H - T \Delta S$
    - H is enthalpy
    - T is temperature
    - S is entropy
  - Proteins fold to reduce their free energy
  - Most of the bonds within a protein have a relatively small enthalpy, so
    folding is driven almost entirely by entropy.
  - While it would seem that entropy would favour a random shape over a small
    subset of folded states, the entropy removed by water cages forming around
    hydrophobic groups on the surface of the protein
