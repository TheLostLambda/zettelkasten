:PROPERTIES:
:ID:       b9a4513b-c995-4cf4-8867-c942b53d6b12
:END:
#+TITLE: RNA Quantification & Sequencing
#+DATE: 2020-10-12 (12:25)
#+ROAM_REF: [[file:~/Documents/University/Y3S1/Genes, Genomes, and Chromosomes/MBB267 Lecture 5 RNA-seq ChIP-seq and 3C 20-21 handout.pdf]]

Tags: [[id:46a40352-2f1a-413b-871a-5ca06ce59a6e][§Genes, Genomes, and Chromosomes]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]]
[[id:a12a79a5-4a3b-49ff-8ece-cd4926a7f495][§Bioinformatics]] [[id:a33e8933-6246-4c37-9201-3a4f33e894c6][§RNA]] [[id:608580ec-a06d-4268-becf-5caa8b021359][§Functional Genomics]]

* Transcriptomics Motivation
  - Looks at the global state of gene expression and allows us characterise the
    expression of any given gene relative to every other gene.
  - Answers which genes are expressed, when they are expressed, and how much
    they are expressed
* Methods
** Northern Blotting
   1) RNA is extracted from a sample
   2) Electrophoresis separates the RNAs by size
   3) They are then blotted onto a membrane
   4) The membrane blot is saturated with radiolabelled RNA probes that hybridise
      with our gene of interest
   5) The probes are washed away and the membrane is imaged with an X-ray film
      so it can be seen which bands were carrying the RNA of interest
*** Diagram
[[file:.orgimg/Transcriptomics_Methods/2020-10-12_12-54-29_screenshot.png]]
** RT-qPCR (Reverse Transcriptase Quantitative PCR)
   1) RNA is extracted from a sample
   2) A primer complementary to the gene of interest allows for the reverse
      transcription of the RNA sample to cDNA
   3) qPCR is carried out by targeting the gene with fluorescent primers that
      only fluoresce once they have been incorporated into the DNA by polymerase
   4) The cycle number at which fluorescence appears is indicative of the
      initial concentration of the targeted RNA
*** Diagram
[[file:.orgimg/Methods/2020-10-12_13-08-34_screenshot.png]]
** Microarrays
   1) RNA is extracted from a sample
   2) DNA probes are designed to hybridise with different gene sequences of
      interest and bound to a glass slide in clusters – this becomes the
      microarray where each cluster is a single target sequence.
   3) The sample RNA is tagged with a fluorescent molecule
   4) The sample RNA is run over the microarray and given the opportunity to
      hybridise with the probes on its surface
   5) The microarray is then fluorescently imaged and each cluster manifests as
      a dot of a different fluorescent intensity. The brighter the dot, the more
      of that gene was expressed in the sample.
*** Limitations
    - Imperfect sequence matches often hybridise with the probes
    - We can't measure the expression of any genes we don't have a probe for
    - Some probe clusters could become saturated, meaning we can't distinguish
      between highly expressed genes
*** Diagram
[[file:.orgimg/Methods/2020-10-12_13-18-05_screenshot.png]]
** RNA-seq
   1) RNA is extracted from a sample
   2) RNA is converted into cDNA and prepared for sequencing
   3) cDNA transcripts are sequenced via a highly parallel sequencing technology
      (such as Illumina)
   4) cDNA reads are then mapped back to a reference genome ([[id:5a179b82-5a75-4a5e-bf3d-66289410c9db][§DNA Resequencing]])
      – the greater the read depth of a gene (the number of reads mapping back
      to it), the more of that gene was being expressed in the sample
*** Complications
    - In eukaryotes, the presence of introns means that alignment software needs to be aware
      that some cDNA reads could span an intron and be split and mapped to
      distant parts of the genome
    - In non-model organisms, there might not be a reference genome, so /de
      novo/ transcriptome assembly can be attempted. Conceptually very similar
      to genome assembly, but the differing number of reads from gene to gene
      becomes significant and several transcripts could arise from alternate
      splicings of a single gene.
*** Comparison to Microarrays
    - RNA-seq has a larger dynamic range then microarrays (a greater ability to
      distinguish different levels of expression)
    - Genome-wide and can detect novel transcripts for which probes may not exist
    - RNA-seq allows for the detection of SNPs
    - RNA-seq can be done without a reference genome
*** Diagrams
[[file:.orgimg/Methods/2020-10-12_13-31-04_screenshot.png]]
[[file:.orgimg/Methods/2020-10-12_13-31-23_screenshot.png]]
