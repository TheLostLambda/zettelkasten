:PROPERTIES:
:ID:       bcf9c75c-b116-48d9-b4f3-c2c926151cc2
:END:
#+TITLE: Dēmos
#+DATE: 2021-06-04 (19:51)

Tags: [[id:2014ada5-6974-47a0-baeb-551d803dd269][§Politics]]

* Overview
  - A fictional nation designed as the ideal democracy
* Background
** The History of Democracy
   - What is it? How does this compare to its modern implementation?
   - Ancient (Greek?)
   - Arises following the downfall of unpopular kings (Athenian revolution)
   - When does a system become a democracy?
** Is Democracy A Good Idea?
   - Is majority rule always a good thing?
** The Plan for the Podcast
   - Three main sections (divisions are naturally blurred):
     - Designing a Constitution (Objectivish?)
     - Suggesting Policy (Subjectivish?)
     - Case-Studies
** The Demographics of Dēmos
   - Scale
   - Time-period
** Threats To Democracy
* Constitutional Sections (More Objective)
** Philosophy
   - Is stability or dynamicism more important?
** Rights of the People
*** Origin of Rights
    - Is it about having the legal sanction to do something, or actually being
      able to exercise that right?
    - Rousseau, Buddhism 
*** Right to Organise
    - Protest
    - Unions
*** Property?
** Branches of Government
*** Checks & Balances
    - Ostracization?
    - Stability and flexibility without collapse
** Allocation of Power
*** How Much Power Should the Government Have?
*** Federalism
    - Levels of power (does this alleviate majority tyranny?)
    - Councils
*** Republicanism
    - Vs direct democracy?
** Elections & Voting Systems
** Finance
*** Taxation
** Amendments
* Foundational Policy (More Subjective)
** Socioeconomic
*** Transport
*** Education
*** Health
** Foreign & Defence
** Policing & Security
*** Division Between Carriers of Force & Civil Servants
** Environmental
** Media & Culture
   - Should the media be policed?
   - """"Cancel Culture""""
** Business & Trade
** Role of the Judiciary in Policy
* Case Studies & Context
** How Technology Affects Democracy
   - Plato's "Philosopher King"
** The Role of Science & Objectivism in Politics
** Other Democratic Constitutions
   - United States
   - Germany
   - Soviet Union (+ Modern Russia)
   - China
** Decline Into Fascism
*** Neoliberalism
    - Vs classical liberalism
*** Nazism
** Origins & Role of Capitalism
** Socialism, Communism, *Anarchism* – Oh My!
*** Sex Pistol Anarchy
    - Aesthetics over idealism?
** Overton Window & "Acceptable" Politics
* Bonus Episodes
** Vexillology
** Fist Fight
** High-Vis Jackets & The Rise to Power
