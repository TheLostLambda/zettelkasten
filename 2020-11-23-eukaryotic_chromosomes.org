:PROPERTIES:
:ID:       10bd1ab3-e0dc-4dee-a91b-b0d9f728e468
:END:
#+TITLE: Eukaryotic Chromosomes
#+DATE: 2020-11-23 (11:54)
#+ROAM_REF: [[~/Documents/University/Y3S1/Genes, Genomes, and Chromosomes/notes mbb267-4 DNA packaging within eukaryotic chromosomes.pdf]]
#+ROAM_REF: [[~/Documents/University/Y3S1/Genes, Genomes, and Chromosomes/slides - DNA packaging within eukaryotic chromosomes.pdf]]
#+ROAM_REF: [[~/Documents/University/Y4S1/Genomic Science/Genome Packing + FISH + 3C.pdf]]

Tags: [[id:46a40352-2f1a-413b-871a-5ca06ce59a6e][§Genes, Genomes, and Chromosomes]] [[id:a47eed36-1ba7-4628-aeee-4d9711fd3ea7][Genomic Science]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]]
[[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]] [[id:737fd2ce-19a8-4548-b4b9-11cbd49a9417][§DNA]] [[id:3c41251c-1713-46c6-b497-b71d749f1ed1][§Eukaryotes]]

* Overview
  - DNA of interphase nuclei is packaged together with histones, non-histone
    proteins, and RNA to form chromatin (which is about 50% protein by mass)
  - This structure can exist in a loose form capable of undergoing
    transcription, or be greatly compacted (as in metaphase) to form "condensed"
    chromosomes
  - This dynamic packing makes it possible to fit the entire genome into a
    nucleus while keeping DNA accessible at the molecular level for
    transcription
** Packing Diagram
[[file:.orgimg/Overview/2020-11-23_12-33-44_screenshot.png]]
* Structure
** Structure of the Nucleosome Particle
   - A limited digestion of chromatin can release the nucleosome "beads"
   - Have an octomeric core, with two H2A/H2B and H3/H4 heterodimers
   - 147bp of DNA make 1.7 left-handed turns around the core
   - H1 is bound to the DNA as it enters and exits the nucleosome
   - The linker DNA varies in length, but is always a multiple of 10 nucleotides
     (which constitutes one full turn of the double helix)
   - Histone proteins can vary: H2AX is involved in repair, H3.3 in non-dividing
     cells, and CENP-A in centromeres
   - The histone proteins also have flexible tails that are not present in the
     diagram below
*** Diagram
[[file:.orgimg/Structure/2020-11-23_12-45-45_screenshot.png]]
** Chromatin Fibres
   - Chromatin extracted under low-salt conditions resembles "beads on a
     string", with DNA wrapped around nucleosomes, structures ~10nm in diameter
   - Under physiological salt conditions, this bead structure packs into a
     thicker, 30nm fibre
   - The 30nm fibres can exist in two different packing forms, a single-helix
     (solenoid), or a two-helix zig-gaz form
   - These fibres are then further packed into higher order structures
*** Diagram
**** Comparison
[[file:.orgimg/Structure/2020-11-23_12-38-21_screenshot.png]]
**** 30nm Structure
[[file:.orgimg/Structure/2020-11-23_12-48-22_screenshot.png]]
** Chromosomal Territories
   - Individual chromosome occupy their own specific area of the eukaryotic
     cell nucleus – they aren't all mixed together
   - Transcription drives part of a chromosome to be decondensed, and this forms
     a loop of loose DNA that leaves the chromosomes traditional territory
   - Many of these transcribed loop segments end up in "transcription
     factories", areas of the nucleus that contain large concentrations of the
     transcriptional machinery needed for gene expression
*** Diagram
[[file:.orgimg/Structure/2020-11-23_13-13-06_screenshot.png]]
** TADs & LADs
   - TADs, or Topologically Associated Domains, are groups of physically
     adjacent genes in a chromosome that might actually be rather distant in
     sequence
   - Genes within a TAD are coordinately regulated and replicated. They are also
     isolated so that, while an enhancer ([[id:6731cc14-75f6-4cfb-aa9a-f524260aca49][§Eukaryotic Gene Promoters]]) can affect
     all of the genes within it's own TAD, it can't affect genes in adjacent
     TADs (though they may interact with TADs farther away in sequence)
   - The boundaries between TADs are established by the CTCF and cohesin
     proteins, which form TAD loops
   - CTCF binding motifs are directional, and TADs seem to form between CTCF
     motifs that point towards each other
   - Between TADs, there can be LADs (lamina associated domains), that are bound
     to the inner wall of the nuclear membrane and contain transcriptionally
     silenced DNA
*** Finding TADs & LADs
    - The primary tool for investigating this is chromatin conformation capture
      like 3C or Hi-C ([[id:4b11c640-5efe-495d-ba1b-bf91bd89dd71][Chromatin Analysis]])
    - [[id:579abbd0-3e99-497a-ac67-7ea73946a6a8][FISH]] and imaging can be used to actually measure the distance between two
      DNA regions (at the cost of a lower base resolution than the sequencing
      based methods)
*** TAD Graphs / Grids (See [[id:10bd1ab3-e0dc-4dee-a91b-b0d9f728e468][Eukaryotic Chromosomes]])
    - Produced using Hi-C data, these grids show how often different parts of
      the genome interact with each other (with darker regions showing a
      stronger interaction)
    - These graphs revealed that there were certain genomic boundaries within
      which genes could contact each other, but not contact regions outside of
      them
    - The locations of these boundaries were found to be preserved between
      humans and mice, indicating that a conserved sequence plays a role here
    - These TADs also contain chromatin modifications, with different TADs
      being differentially modified from their neighbours
    - TADs can also have within them smaller sub-TADs. The corners of these
      TADs and sub-TADs may represent sites of pinching to form a loop, so they
      show more contact than usual
**** Diagrams
***** Basic Example
[[file:.orgimg/Enhancers/2022-01-25_16-44-36_screenshot.png]]
***** Real TADs and Chromatin Modification Containment
[[file:.orgimg/Enhancers/2022-01-25_16-48-56_screenshot.png]]
***** Loop Domains & Sub-TADs
[[file:.orgimg/Enhancers/2022-01-25_16-53-26_screenshot.png]]
*** Loop Extrusion Model of TAD Formation
    - TADs aren't actually stable structures, but rather the result of
      statistical averaging
    - Cohesin pulls loops of DNA through it until CTCF stops the coheasin from
      pulling things any farther, forming a TAD and the looped domains within it
    - Cohesin then falls off and repeats the process elsewhere
**** Diagrams
***** TADs, Loop Domains, and Sub-TADs
[[file:.orgimg/Structure/2022-01-25_17-06-19_screenshot.png]]
***** Loop Extrusion
[[file:.orgimg/Structure/2022-01-25_17-12-13_screenshot.png]]
*** Genome Compartments / Territories
  - Because distant TADs can interact, there appear to be "compartments" for
    gene expression and repression with regions of DNA that move between the two
    in different cell types
  - On an even bigger scale, you generally don't get inter-chromosomal
    interactions
  - Chromosome painting can be used to visualise different chromosomes in the
    nucleus (via [[id:579abbd0-3e99-497a-ac67-7ea73946a6a8][FISH]])
  - Gene rich chromosomes tend to be towards the centre with their gene-rich
    regions pointing that direction. Gene-poor regions associate with the
    nuclear membrane
  - Chromosomes have their own sort of territories in the nucleus
**** Diagrams
***** Genome Compartments & Cross TAD Interactions
[[file:.orgimg/Structure/2022-01-25_17-14-56_screenshot.png]]
***** Chromosome Territories
[[file:.orgimg/Structure/2022-01-25_17-15-30_screenshot.png]]
***** Chromosome Painting
[[file:.orgimg/Structure/2022-01-25_17-17-08_screenshot.png]]
*** Diagrams
[[file:.orgimg/Structure/2020-11-23_13-20-18_screenshot.png]]
[[file:.orgimg/Structure/2022-01-25_17-17-36_screenshot.png]]
** Histone Modifications
   - The tails of core histone proteins can be post-transcriptionally modified
     in a number of ways
   - These modifications include acetylation (Lys), Methylation (Lys, Arg),
     Phosphorylation (Ser, Thr), and ubiquitylation (Lys)
   - These modifications can change the way that histones interact with each
     other and other proteins, directly affecting chromatin structure and gene
     expression
   - Some modifications are mutually exclusive (K-Ac; K-me), or one can be a
     prerequisite for another (H2-Ub, H3K4me)
*** Lysine Modifications
    - Many important histone modifications involve the \epsilon-amino group of lysines
      (the one on the side-chain)
    - Histone acetyltransferases neutralise the positive charge on lysine side
      chains. This modification is readily reversed by histone deacetylase
      complexes (HDACs)
    - Modification by histone methyl-transferases (HMTs) blocks
      acetylation. Methyl groups can be slowely removed by lysine-specific
      demethylases (LSDs)
**** Diagram
[[file:.orgimg/Structure/2020-11-23_13-39-00_screenshot.png]]
*** Diagram
[[file:.orgimg/Structure/2020-11-23_13-36-12_screenshot.png]]

