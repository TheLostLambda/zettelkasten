:PROPERTIES:
:ID:       96e9590d-3ae5-41da-9501-7e6a8c0fa9d6
:END:
#+TITLE: Synthetic Biology
#+DATE: 2021-01-13 (20:43)

* Resources
** Journals
   - [[https://www.sciencedirect.com/journal/current-opinion-in-systems-biology/special-issue/10PP5SZ8RZR][Current Opinion in Systems Biology]]
** Videos
   - [[https://www.youtube.com/channel/UCV5vCi3jPJdURZwAOO_FNfQ][The Thought Emporium]]
** People
   - Jens Nielsen
** Institutions
   - Max Planck Institute
** Books
   - Metabolic Engineering Principles and Methods by Jens Nielsen
