:PROPERTIES:
:ID:       8e4e0020-fd32-44c8-acd6-8fef074d2034
:END:
#+TITLE: Growbotic Systems
#+DATE: 2021-09-24 (13:20)

* Minutes
** 2021-09-24
   - Start with idealised only approach to the graph and protocol (step-response)
   - Add a spectra part of the protocol that can be referenced in light steps
   - Maybe have a sort of mixing graphic for the spectrum
   - Use the FAT tests as a way of assessing protocol viability
   - Implement actions like imaging – plopping some action name into the CSV
   - Put ranges of valid values in the system specification file
** 2021-10-20
*** Topics
    - Get Gareth up to speed with compilation and JS integration
    - Check FAT tests
    - Decide on how photos should be taken
    - Should units be translated by the compiler? What happens when units are
      hard to convert (flood time vs ml of water, or PAR to PFD?)
    - Should we move from parsing TOML to KDL? It might be much easier to extend
      in the future...
    - Add latency or exclusion periods in the settings file (preventing
      scheduling certain events too close to each other)
    - Settings file could store constraints of systems (how fast can we reach a
      set point, for example)
    - Add "recipes" for different light spectra and image steps
    - Make sure that the =time= variable is available in the formulas
    - Settings file could have a ramping / setpoint rate
    - Describe light in 5-7 bands, total brightness, and percent composition or
      something similar (like flex-box fractions)
    - Settings file could have a list of bands / LEDs. These can be set to a
      certain power in the protocol
    - For pictures:
      - Type: Normal, Multispectral (with bands)
      - Light: How to change it
      - Exposure: How long to take the picture
    - Write a set of minimum examples
* Tasks
** TODO Enable nice graphs to be produced with the website
** TODO Allow the specification of light spectra (split things into bands)
   - UV
   - B
   - G
   - R
   - FR
   - IR

