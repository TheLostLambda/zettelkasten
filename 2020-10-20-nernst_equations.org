:PROPERTIES:
:ID:       a26c207b-172f-44df-92ce-d87ad5d913a0
:END:
#+TITLE: Nernst Equations
#+DATE: 2020-10-20 (19:53)
#+ROAM_REF: [[file:~/Documents/University/Y3S1/Biostructures, Energetics, and Synthesis/MPW Lecture 8.pdf]]

Tags: [[id:aca6de40-3d93-439b-aa1a-421b458e28d8][§Biostructures, Energetics, and Synthesis]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]]

* [[id:7f3afc64-4ab6-44f7-afc7-6150b20d8ae6][§Biological Membrane]] Potential
  - A difference in either concentration or charge across a membrane can store
    free energy – ion gradients combine both of these
  - These gradients are directly responsible for driving many cellular processes
* Equations
** Potential -> Energy
   - $\Delta G = zFE$
   - \Delta{}G is free energy
   - z is the charge per ion
   - F is the Faraday constant (96485 J mol^{-1} V^{-1})
   - E is membrane potential (in V)
** Concentration -> Energy
   - $\Delta G = RT \ln{\left(\dfrac{\text{[out]}}{\text{[in]}}\right)}$
   - \Delta{}G is free energy
   - R is the gas constant (8.31 J K^{-1} mol^{-1})
   - T is the temperature in Kelvin (0°C = 273.15K)
   - [out] is the concentration outside the membrane
   - [in] is the concentration inside the membrane
** Concentration -> Potential (Nernst Equation)
   - $E = \dfrac{RT}{zF}\ln{\left(\dfrac{\text{[out]}}{\text{[in]}}\right)}$
   - E is membrane potential (in V)
   - R is the gas constant (8.31 J K^{-1} mol^{-1})
   - T is the temperature in Kelvin (0°C = 273.15K)
   - z is the charge per ion
   - F is the Faraday constant (96485 J mol^{-1} V^{-1})
   - [out] is the concentration outside the membrane
   - [in] is the concentration inside the membrane
** Concentration + Potential -> Energy
   - $\Delta G = RT \ln{\left(\dfrac{\text{[out]}}{\text{[in]}}\right)}-zFE$
   - \Delta{}G is free energy
   - R is the gas constant (8.31 J K^{-1} mol^{-1})
   - T is the temperature in Kelvin (0°C = 273.15K)
   - [out] is the concentration outside the membrane
   - [in] is the concentration inside the membrane
   - z is the charge per ion
   - F is the Faraday constant (96485 J mol^{-1} V^{-1})
   - E is membrane potential (in V)
