:PROPERTIES:
:ID:       d41ca026-a0e7-45e0-820d-52b40efc9d00
:END:
#+TITLE: Lipids
#+DATE: 2020-10-18 (14:34)
#+ROAM_REF: [[file:~/Documents/University/Y3S1/Biostructures, Energetics, and Synthesis/MPW lecture 3.pdf]]

Tags: [[id:aca6de40-3d93-439b-aa1a-421b458e28d8][§Biostructures, Energetics, and Synthesis]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]]

* Primary [[id:7f3afc64-4ab6-44f7-afc7-6150b20d8ae6][§Membrane]] Lipids
** Phospholipid
   - R-group + Phosphate head, then glycerol with two fatty acid chains
   - When unsaturated, the double bonds are usually cis, producing a kink in the tail
*** Diagram
[[file:.orgimg/Primary_\[\[file:2020-10-18-biological_membranes.org\]\[§Membrane\]\]_Lipids/2020-10-18_14-45-16_screenshot.png]]
** Sphingolipid
   - Head can be R-group + phosphate or a sugar complex
   - The two fatty acid chains are held together by an NH instead of an O, so
     there is no longer a glycerol in the structure
   - The chains are usually saturated, but if they aren't, they are trans
     double-bonds, so there are no kinks in the chains
*** Diagram
[[file:.orgimg/Primary_\[\[file:2020-10-18-biological_membranes.org\]\[§Membrane\]\]_Lipids/2020-10-18_14-48-36_screenshot.png]]
** Sterols
   - Shorter, fatter lipids containing ring structures
   - Cholesterol is the most prevalent example
*** Diagram
[[file:.orgimg/Primary_\[\[file:2020-10-18-biological_membranes.org\]\[§Membrane\]\]_Lipids/2020-10-18_14-59-15_screenshot.png]]
* Consistency
  - Membranes of many trans-saturated lipids have fatty-acid chains that pack
    quite well, resulting in a hard, gel-like consistency.
  - Membranes with more cis-double bonds tend to be more fluid (most biological
    membranes are fluid, so proteins can be mobile)
  - Regions of cis-membrane are often thinner than the well-packed, quite
    straight trans regions
  - Cholesterol is a flat lipid that can straighten out the chains of cis-lipids
    – slightly growing the thickness of the membrane
** Diagrams
*** Packing
[[file:.orgimg/Consistency/2020-10-18_15-10-17_screenshot.png]]
*** Cholesterol Effect
[[file:.orgimg/Consistency/2020-10-18_15-10-41_screenshot.png]]
* Shape
  - The different R-groups on the heads of phospholipids come in different sizes
  - Phosphatidylcholine (PC) is largely cylindrical (with an R-group the same
    size as the fatty-acid tail), but something like phosphatidylethanolamine
    (PE) has a quite small head group.
  - This smaller head group means that that outer surface of the membrane needs
    to pack more tightly where those lipids are present – this produces a
    curvature
  - Other methods include helix insertion, scaffolding, membrane proteins, or
    cytoskeletal shaping
** Diagrams
*** Head Groups
[[file:.orgimg/Shape/2020-10-18_15-16-04_screenshot.png]]
*** Curvature
[[file:.orgimg/Shape/2020-10-18_15-30-41_screenshot.png]]
*** Other Curvature Methods
[[file:.orgimg/Shape/2020-10-18_15-44-02_screenshot.png]]
