:PROPERTIES:
:ID:       f01cadef-b213-478b-985e-258d3515a03a
:END:
#+TITLE: Traceback
#+DATE: 2020-11-21 (21:45)
#+STARTUP: entitiesplain

Tags: [[id:e33f8645-5c24-4c52-855f-289a9a5c99c4][§Computing]] [[id:19fab79e-8fc9-492e-b68c-e2cb9ef69d4e][§Hacking]] [[id:cd4fc7d3-0762-49ed-b17b-e41fd0ef1088][§Hack The Box]]

* Path
  1) nmap
  2) inspect html
  3) find webshell comment
  4) find [[https://github.com/TheBinitGhimire/Web-Shells][xh4h web shell]]
  5) gobuster on the shell names in that repo
  6) Head to smevk.php
  7) Drop my public SSH key into webadmin/.ssh/authorized_keys
  8) Snoop the bash history
  9) sudo -l
  10) Write a lua script with os.execute to copy authorized_keys to sysadmin
  11) sudo -u sysadmin /home/sysadmin/luvit
* Notes
** nmap -sC -sV
Starting Nmap 7.91 ( https://nmap.org ) at 2020-11-21 21:54 GMT
Nmap scan report for 10.10.10.181
Host is up (0.027s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 96:25:51:8e:6c:83:07:48:ce:11:4b:1f:e5:6d:8a:28 (RSA)
|   256 54:bd:46:71:14:bd:b2:42:a1:b6:b0:2d:94:14:3b:0d (ECDSA)
|_  256 4d:c3:f8:52:b8:85:ec:9c:3e:4d:57:2c:4a:82:fd:86 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Help us
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 8.03 seconds
** find / -group sysadmin
/etc/update-motd.d
/etc/update-motd.d/50-motd-news
/etc/update-motd.d/10-help-text
/etc/update-motd.d/91-release-upgrade
/etc/update-motd.d/00-header
/etc/update-motd.d/80-esm
/home/sysadmin
/home/sysadmin/.bashrc
/home/sysadmin/luvit
/home/sysadmin/.bash_logout
/home/sysadmin/.ssh/authorized_keys
/home/sysadmin/.cache
/home/sysadmin/.cache/motd.legal-displayed
/home/sysadmin/.bash_history
/home/sysadmin/user.txt
/home/sysadmin/.local
/home/sysadmin/.local/share
/home/sysadmin/.local/share/nano
/home/sysadmin/.profile
/home/webadmin
/home/webadmin/note.txt
