:PROPERTIES:
:ID:       d60fbcfc-ab56-4d80-8ccf-bc5f4c6c8291
:END:
#+TITLE: Patch Clamping
#+DATE: 2020-10-18 (17:41)
#+ROAM_REF: [[file:~/Documents/University/Y3S1/Biostructures, Energetics, and Synthesis/MPW Lecture 6.pdf]]

Tags: [[id:aca6de40-3d93-439b-aa1a-421b458e28d8][§Biostructures, Energetics, and Synthesis]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]]

* What is it?
  - An experimental method for investigating the properties of cellular
    ion-channels
  - It runs a current through a patch of membrane using a clamped (fixed)
    voltage
* How does it work?
  1) A glass pipette containing an electrode is set up in a rig with a
     microscope (for seeing the tip of the pipette) and a mounted flow-cell
     containing some cells
  2) The cells are prodded with the pipette to transfer some of the cell
     membrane onto the pipette tip
     - If the cell is prodded, then the tip is pulled away, then the membrane is
       transferred in an inside-out manner (the inside of the cell membrane is
       now on the outside of the pipette)
     - If after the cell is prodded, some liquid is aspirated, then the membrane
       separating the cell from the fluid in the pipette is broken. This
       orientation effectively makes the whole cell volume continuous with the
       pipette
     - Pulling away after an aspiration results in an outside-out orientation
       (the outside of the cell on the outside of the pipette)
  3) Once there is a membrane over the end of the pipette, an ionic current
     between the electrode in the pipette and one in the flow-cell can be
     measured.
     - When the ion channels in the transferred region of membrane are open,
       there is a detectable (if quite small, in the pA) current through the
       channel. If the channel is closed, there is no current.
     - For gated channels, different molecules can be run through the flow cell
       and given the chance to interact with the membrane proteins. In this way,
       the molecules that open and close channel can be elucidated
** Diagram
[[file:.orgimg/How_does_it_work?/2020-10-18_18-21-35_screenshot.png]]
