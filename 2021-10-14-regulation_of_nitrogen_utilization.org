:PROPERTIES:
:ID:       7a56ed09-11ad-4863-ae10-d72030ff9881
:END:
#+TITLE: Regulation of Nitrogen Utilization
#+DATE: 2021-10-14 (11:00)
#+ROAM_REF: [[~/Documents/University/Y4S1/Microbial Structure and Dynamics/Nitrogen Regulation.pdf]]

Tags: [[id:9cd2aa9d-f592-49ce-ba43-0a3765723cff][Microbial Structure and Dynamics]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][Biology]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][University]]

* Overview
** Example
   - Enterics (E. coli)
   - Utilize NH_3 (ammonia) as a sole N source
** Paths of Nitrogen Entry
   1) NH_3
   2) Glutamine > purines, pyrimidines, etc
   3) Glutamate > most amino acids
** Environmental Sensing
   - NH_3 > 1mM :: Produces Glutamate, Glutamine, Asparagine
   - NH_3 < 0.1mM :: Produces only glutamine
** Mechanism
   1) Low NH_3 -> low glutamine / glutamate : \alpha{}ketoglutarate ratio
   2) UTase stimulated
   3) UTase uridylylates protein P_II (glnB in /E. coli/ – a different operon?)
   4) P_II interacts with Nr_II and acts as a phosphatase – preventing Nr_II from
      becoming phosphorylated
   5) P_II(UMP)_4, however, does not interact with Nr_II
   6) Unbound Nr_II acts as a kinase and autophosphorylates
   7) Nr_II transfers its phosphate to Nr_I
   8) Nr_I-P allows open complex formation (only phosphorylated Nr_I can bind the
      enhancer site)
   9) Bound Nr_I can interact with \sigma^54 and lead to open complex formation
*** Diagrams
*** High NH_3 Conditions
[[file:.orgimg/Overview/2022-01-05_12-59-36_screenshot.png]]
*** Low NH_3 Conditions
[[file:.orgimg/Overview/2022-01-05_13-00-01_screenshot.png]]
* Nitrogen Assimilation Reactions
** Glutamine Synthetase
   - NH_3 + Glutamate + ATP -> Glutamine + ADP + Pi
   - Requires energy (ATP) to fix into glutamine
   - See [[id:9c8a5b39-5b46-4b8c-b261-388ed1905cc9][Biosynthesis of Amino Acids]]
** Glutamate Synthase
   - Glutamine + 2-Ketoflutarate + NADPH -> 2 Glutamate + NADP^{+}
** Glutamate Dehydrogenase
   - NH_3 + 2-Ketoglutarate + NADPH -> Glutamate + NADP^{+}
   - Replaces the two-step Glutamine Synthetase + Glutamate Synthase reaction
     with a single-step reaction
   - While this is a nice reaction that doesn't require ATP, it only works at
     higher concentrations of NH_3 (Has a K_m of 1mM so it's ineffective at low
     concentrations)
* Nitrogen Regulation
** Response to Low NH_3 Conditions (<1mM)
   1) The ammonia assimilatory cycle (obtain more NH_3 from the environment)
      - Does this via the energy consuming Glutamine synthetase + Glutamate
        synthase reaction
   2) Alternative (non-NH_3) nitrogen sources can be utilised
      - /hut/ operon for histidine utilisation in /Klebsiella aerogenes/
   3) Some organisms can fix inorganic nitrogen (N_2 -> NH_3)
      - This is quite rare because its a complex, energy intensive process
*** Diagrams
**** Ammonia Assimilatory Cycle
[[file:.orgimg/Nitrogen_Regulation/2022-01-05_11-23-13_screenshot.png]]
**** N-Reg Response Pathways
[[file:.orgimg/Nitrogen_Regulation/2022-01-05_11-23-52_screenshot.png]]
** Ntr (Nitrogen Regulated) System
   - Is a master regulator of all of the low-N pathways
   - Approximately 100 genes in /E. coli/ – lots are genes are involved in the
     response to low-N conditions!
*** Control of Glutamine Synthetase Activity
    - Don't use this when NH_3 is available!
    - Can be controlled at several different levels
      1) Feedback inhibition by products of glutamine metabolism, like histidine
         - High His means high glutamine, so no more activity needed)
      2) Post-translational modification (adenylation)
         - Each of 12 subunits can be adenylated, inactivating that subunit and
           enhancing the susceptibility of the rest to feedback inhibition
      3) Gene expression
         - Glutamine synthetase is the product of the /glnA/ gene (in /E. coli/)
**** Genetic Regulation
     - gln is a tricistronic operon (glnA-glnL-glnG) in /E. coli/
     - Three genes and three promoters
***** High NH_3 Conditions
      - Two of the three promoters are active
      - p and p_1 are active and \sigma^A controlled (the housekeeping sigma factor)
      - These maintain a basal level of transcription – produces a small,
        steady-state amount of the enzyme
      - p_1 promotes the expression of the enzyme, glnA, and p codes for
        regulatory factors glnL and glnG
****** Diagram
[[file:.orgimg/Nitrogen_Regulation/2022-01-05_11-54-25_screenshot.png]]
***** Low NH_3 Conditions
      - Instead of two transcripts, for the enzyme and other components,
        a single tricistronic transcript is produced
      - Promoter p_2 is used and is controlled by an alternative sigma factor
        \sigma^54 - rpoN
      - \sigma^54 isn't like \sigma^A that can just bind the promoter and start
        transcription, it requires an "enhancer" (glnG) to initiate
        transcription
      - When this promoter is active, a greater amount of glnA is produced
****** Diagram
[[file:.orgimg/Nitrogen_Regulation/2022-01-05_12-01-13_screenshot.png]]
***** Nr_I Enhancer (glnG)
      - Part of the same operon that it regulates
      - Enhancer allows for "open complex" formation at the promoter (strand
        separation and start of transcription)
      - Enhancer binds to an upstream enhancer site, loops the DNA, then
        interacts with the \sigma^54 in the RNA polymerase sitting at the promoter
        site
****** Regulation
       - The Nr_I protein can only bind and act as an enhancer when it's
         activated via phosphorylation
       - Controls the whole Ntr system – ammonia assimilatory cycle, alternative
         nitrogen sources, nitrogen fixation
       - A sensor protein (Nr_II) phosphorylates the regulator protein (Nr_I),
         activating it when needed
****** Diagram
[[file:.orgimg/Nitrogen_Regulation/2022-01-05_12-11-58_screenshot.png]]
***** Nr_II Sensor (glnL)
      - A sensor protein, but only has a transmitter domain (the histidine
        kinase that autophosphorylates) and is cytosolic (not membrane bound)
      - Nr_II is not then directly controlled by the environment; but rather,
        controlled by a phosphatase P_II (which prevents autophosphorylation)
      - P_II itself is subject to a post-translational uridylylation modification
        (to same one that regulates Glutamine Synthetase in the [[id:9c8a5b39-5b46-4b8c-b261-388ed1905cc9][Biosynthesis of
        Amino Acids]]) – when this modification is present, it can't bind Nr_II to
        deactivate it
      - A UR/UTase (an enzyme with dual uridylyl removing and uridylyl
        tranferase activities) adds and removes this modification on P_II
      - Glutamine binds to the UR/UTase and stimulates the URase (uridylyl
        removing activity), while a lack of glutamine stimulates the UTase
        activity (activating uridylylation)
* Nitrogen Fixation
  - N_2 reduced to NH_3 via the nitrogenase enzyme
  - The enzyme is very sensitive to O_2 (won't work in the presence of O_2)
  - /Klebsiella pneumoniae/ has approximately 20 genes involved in fixation
  - Ntr system also regulates the expensive process of fixation via the Nif
    regulon (nitrogen fixation)
  - Nif is controlled by NtrA (\sigma^54 / rpoN)
  - NtrC acts like Nr_I (the enhancer)
  - Low NH_3 leads to transcription of the nifLA operon, but *not* the rest of
    the /nif/ genes containing the fixation machinery
  - NifA is a regulator (enhancer) for the smaller Nif regulon
  - NifA acts like NtrC / Nr_I and activates NtrA to transcribe the other nif
    genes. NtrC turns on NifA, NifA turns on the rest of /nif/
  - NifA itself is regulated by NifL (which measures O_2 and fixed nitrogen
    levels).
  - NifL is a very sensitive sensor of oxygen which represses transcription via
    nifA when O_2 is present
  - Ultimately, when O_2 is low and NH_3 is as well, nitrogen fixation is turned on
* Questions
  - How is the Adenylation controlled / connected to the glutamine levels?
    - Not the same UTase system as the gene-control, but it's a similar sort of
      system
