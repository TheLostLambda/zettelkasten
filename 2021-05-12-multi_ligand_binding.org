:PROPERTIES:
:ID:       1866b384-fe5d-475d-82e6-1949b646ceef
:END:
#+TITLE: Multi-Ligand Binding
#+DATE: 2021-05-12 (19:01)
#+ROAM_REF: [[~/Documents/University/Y3S2/Biochemistry/Lectures/MBB261_CJC_RAS_two_ligands_inhibition_etc_slides_2017.pdf]]

Tags: [[id:33ee1804-bcfb-40c5-8f2c-62d54324c757][§Biochemistry 2]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]] [[id:40afa1ec-8f37-4248-9c3c-2bc817e2ee2c][§Reaction Kinetics]]

* Overview
  - Many proteins have more than one ligand binding site
  - The primary ligand binding site is referred to as the orthosteric site and
    the secondary binding site is referred to as the allosteric site
* Binding Two Different Ligands
** Distant Binding Sites
   - The binding at one site is unlikely to affect the other site
   - Binding is independent and the binding of one ligand, does not affect the
     binding of another
*** Diagram
[[file:.orgimg/Binding_Two_Different_Ligands/2021-05-12_20-10-34_screenshot.png]]
** Adjacent Binding Sites
   - Binding can be independent, positively cooperative, or negatively
     cooperative
   - When there is no particular clash or attraction between the ligands,
     binding appears independent
   - When there is some attraction between the two ligands, they exhibit
     positive cooperative binding
   - When there is a clash or repulsion between the two ligands, negative
     cooperative binding is observed
*** Diagram
[[file:.orgimg/Binding_Two_Different_Ligands/2021-05-12_20-52-51_screenshot.png]]
** Shared Binding Site
   - Binding is competitive mutually exclusive (an extreme form of negatively
     cooperative binding)
   - The only way to realistically distinguish competitive binding from
     extremely negative cooperative binding is to incorporate structural
     information
*** Diagram
[[file:.orgimg/Binding_Two_Different_Ligands/2021-05-12_20-53-03_screenshot.png]]
** (Cooperative) Conformational Allostery
   - The binding of one ligand in a non-ideal binding pocket improves the
     binding pocket for a second ligand
   - This interaction is bidirectional, so the binding of the second ligand
     further enhances the binding pocket of the first
*** Observed Change in K_D
    - If the non-independent ligands L and M bind the protein P, then the K_D of
      L varies with the concentration of M
    - If the ligands bind independently, then the K_D of L remains unchanged
      despite the addition of M
    - If they are negatively cooperative, M will increase the K_D of L
    - If they are positively cooperative, M will decrease the K_D of L
**** Experimental Determination
     1) Measure a binding curve
     2) Add a saturating concentration of the second ligand
        - Once increasing the concentration of second ligand no longer affects
          the K_D of the first, you know it's saturating
     3) Measure a new binding curve and calculate the different, adjusted K_D
*** Diagram
[[file:.orgimg/Binding_Two_Different_Ligands/2021-05-12_20-56-40_screenshot.png]]
* Binding Two (Or More) of the Same Ligand
** Haemoglobin & Myoglobin
   - When a ligand binds a protein multiple times with some sort of cooperative
     effect, the binding curve changes shape from a regular hyperbolic shape to
     something more sigmoidal
   - A good example of this is myoglobin (Mb) and haemoglobin (Hb) – both bind
     oxygen, but myoglobin is monomeric while haemoglobin is a tetramer
     exhibiting positive cooperative binding
   - The cooperativity of haemoglobin binding allows it to transfer either to or
     away from myoglobin depending on the partial pressure of O_2
*** Diagrams
[[file:.orgimg/Binding_Two_(Or_More)_of_the_Same_Ligand/2021-05-13_14-10-17_screenshot.png]]
[[file:.orgimg/Binding_Two_(Or_More)_of_the_Same_Ligand/2021-05-13_14-52-13_screenshot.png]]
** (Cooperative) Conformational Allostery
   - The sigmoidal curve comes from a reduced K_D after the first ligand has
     bound – the average affinity (the slope of the curve, very loosely
     speaking) then increases as ligand concentration does before flattening
     back out near saturation
   - The overall binding curve then falls between the two hypothetical binding
     curves where the second site is either always empty or always full
   - Independent binding shows no relation between the sites-bound and K_D, but
     *extreme* (not quite realistic) cooperativity shows no K_D change until the
     penultimate ligand binds, at which point the K_D drops dramatically
*** Hills Equation
    - The hill coefficient (n_h) is a measure of how cooperative the multi-ligand
      binding is. A value of 1 represents independent binding and an n_h matching
      the number of ligand binding sites represents extreme cooperativity –
      typically, the true n_h sits somewhere in the middle of the two
    - The larger the n_h, the sharper the "switching" behaviour of the binding
**** Equation
     - $\text{Average # of sites bound} = \text{N\textsubscript{sites}} \times \dfrac{[L]^{n_h}}{[L]^{n_h} + {K_D}^{n_h}}$
     - N_sites is the total number of sites available for ligand binding
     - [L] is the ligand concentration
     - K_D is the apparent dissociation constant of the interaction
     - n_h is the hill coefficient (ranging from 1 to N_sites)
**** Diagram
[[file:.orgimg/Binding_Two_(Or_More)_of_the_Same_Ligand/2021-05-13_14-51-17_screenshot.png]]
*** Diagrams
[[file:.orgimg/Binding_Two_(Or_More)_of_the_Same_Ligand/2021-05-13_14-17-42_screenshot.png]]
[[file:.orgimg/Binding_Two_(Or_More)_of_the_Same_Ligand/2021-05-13_14-22-04_screenshot.png]]
** Binding to Enzymes & Receptors
   - Enzymes bind their substrate (the orthosteric ligand) and another ligand,
     which can be either a competitive inhibitor at the same site, or an
     allosteric modulator that binds at a separate site
*** Competitive Inhibition
    - On the level of individual enzymes, the activity is either unaffected or
      entirely arrested
    - On the whole then, the k_cat (and therefore V_max) remains unchanged, but
      the addition of an inhibitor competes with the substrate for binding and
      increases the K_M ([[id:98c553df-b8b2-4580-a91b-67714eb8e675][§Enzyme Kinetics]])
    - A high-enough substrate concentration can still push the enzyme activity
      to V_max
    - The dissociation constant between an enzyme and its *inhibitor* is called K_i
**** Equation
     - $\text{rate} = E_{tot} \times k_{cat} \times \dfrac{[S]}{[S] + K_M^{app}}$

     - $K_M^{app} = K_M \times \left(1 + \dfrac{[I]}{K_i}\right)$

     - E_tot is the total amount of enzyme
     - k_cat is the rate constant of catalysis
     - [S] is the substrate concentration
     - [I] is the inhibitor concentration
     - K_M is the Michaelis-Menten constant
     - K_i is the dissociation constant of the inhibitor enzyme complex
*** Allosteric Modulation
    - Binding of the modulator can both change the K_D of the substrate and the
      k_cat of the enzyme (by affecting the conformation of the active site)
    - Allosteric modulation can result in a much more diverse range of effects,
      commonly affecting both the K_M and V_max simultaneously and allowing for
      both inhibition and activation
*** Receptors
    - "Substrate" is referred to as the agonist, and "inhibitors" become
      antagonists
    - While the enzymes respond rather linearly to inhibitor and substrate
      concentrations, the cascading character of most signalling pathways means
      that adding more agonist might not always result in a greater signalling
      response – the same goes for inhibition and antagonists
