:PROPERTIES:
:ID:       f0ff5516-2aad-49ae-9977-6840e4c6a872
:END:
#+TITLE: 3D Printing
#+DATE: 2021-02-16 (20:48)
#+ROAM_REF: file:~/Documents/University/Miscellaneous/Societies/Bionics/MEC454-Basic-principles-of-AM-post-processing-pdf-version1.pdf
#+ROAM_REF: file:~/Documents/University/Miscellaneous/Societies/Bionics/MEC454-Basic-principles-of-AM-pre-processing-pdf-version.pdf
#+ROAM_REF: file:~/Documents/University/Miscellaneous/Societies/Bionics/prusa3d_manual_mini_en.pdf


* Tips
  - Keep the limitations of your manufacturing method in mind when designing a
    part
* Common Types
** Fused Filament Fabrication (FFF) / Fused Deposition Modelling (FDM)
   - Extrudes a molten polymer filament using a precisely positioned nozzle,
     building up a 3D shape out of many small layers
   - Cheap, easy, and suitable for multiple materials at the expense of detail
*** Pictures
[[file:.orgimg/Common_Types/2021-02-16_21-45-28_screenshot.png]]

[[file:.orgimg/Common_Types/2021-02-16_21-47-05_screenshot.png]]
** Stereolithography (SLA)
   - Selectively solidifies parts of a photopolymer by exposing it to UV light
   - Much finer resolution than FFF (down to 0.1mm) with fewer supports, but
     with a complex post-processing involving cleaning and curing
   - Also much more expensive than FFF
*** Pictures
[[file:.orgimg/Common_Types/2021-02-16_22-05-12_screenshot.png]]
[[file:.orgimg/Common_Types/2021-02-16_22-05-46_screenshot.png]]
* STL Files
  - Represents 3D shapes as a triangular mesh
  - Retains only surface geometry information
  - Curves need to be approximated, so you'll need to specify a tolerance
** Tolerance Visualisations
[[file:.orgimg/STL_Files/2021-02-16_21-27-29_screenshot.png]]
[[file:.orgimg/STL_Files/2021-02-16_21-27-59_screenshot.png]]
* Print Orientation
  - Vertically oriented parts are usually the most precise, with the nicest finish
  - For SLA printing, build height is directly related to build time – it's a
    bit more complex for FFF printing.
** Pictures
*** Stair Stepping
[[file:.orgimg/Print_Orientation/2021-02-16_22-07-45_screenshot.png]]
[[file:.orgimg/Print_Orientation/2021-02-16_22-08-02_screenshot.png]]
* Supports
  - Needed to support over-hanging geometries before the polymer is set
  - Often formed using weak lattices or a separate support material so that they
    can be removed after the part has finished printing
  - PrusaSlicer has options for supports on the build-plate, everywhere, or only
    where enforcers are present
** Pictures
[[file:.orgimg/Supports/2021-02-16_22-12-19_screenshot.png]]
* Surface Adhesion
  - Brims can be added to parts to increase the surface area attaching them to
    the build plate
  - Brims stick parts down better and decrease warping when printing using some
    polymers (ABS contracts more as it cools than PLA)
* Layer Height
  - Can affect surface finish and curves – forming a trade-off with build time
** Pictures
[[file:.orgimg/Layer_Height/2021-02-16_22-15-43_screenshot.png]]
* Tolerance
* Print Time
  - Can be sped up by using larger layers or moving the print-head faster, these
    both reduce quality somewhat
** Pictures
[[file:.orgimg/Print_Time/2021-02-16_22-37-39_screenshot.png]]
* Infill
  - FFF printed models are not solid, but filled with some sort of pattern
  - Different patterns have different benefits – some are stronger, others save
    more material
  - The denser the infill, the stronger the part (generally)
* Materials
  - A nice Prusa guide here: https://help.prusa3d.com/en/materials
** PLA
[[file:.orgimg/Materials/2021-02-16_22-53-20_screenshot.png]]
[[file:.orgimg/Materials/2021-02-16_22-53-50_screenshot.png]]
[[file:.orgimg/Materials/2021-02-16_22-55-34_screenshot.png]]
** PET / PETG
[[file:.orgimg/Materials/2021-02-16_22-55-06_screenshot.png]]
[[file:.orgimg/Materials/2021-02-16_22-55-51_screenshot.png]]
[[file:.orgimg/Materials/2021-02-16_22-56-30_screenshot.png]]
** ASA / ABS
[[file:.orgimg/Materials/2021-02-16_22-56-58_screenshot.png]]
[[file:.orgimg/Materials/2021-02-16_22-57-13_screenshot.png]]
