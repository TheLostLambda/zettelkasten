:PROPERTIES:
:ID:       395d3975-df07-4913-9c4f-8da7a2dd9938
:END:
#+TITLE: Eukaryotic Transcription Factors
#+DATE: 2020-11-22 (15:25)
#+ROAM_REF: [[~/Documents/University/Y3S1/Genes, Genomes, and Chromosomes/notes - eukaryotic transcription factors.pdf]]
#+ROAM_REF: [[~/Documents/University/Y3S1/Genes, Genomes, and Chromosomes/slides - eukaryotic transcription factors.pdf]]

Tags: [[id:46a40352-2f1a-413b-871a-5ca06ce59a6e][§Genes, Genomes, and Chromosomes]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]] [[id:737fd2ce-19a8-4548-b4b9-11cbd49a9417][§DNA]] [[id:1d262d6a-6d81-432c-99fe-85823f1110cd][§Gene Regulation]] [[id:3c41251c-1713-46c6-b497-b71d749f1ed1][§Eukaryotes]]

* Overview
  - Eukaryotic specific transcription factors (sTFs) can transcriptionally up or
    down regulate a specific gene or group of genes (regulon)
  - There are two mechanisms for accomplishing this: sTF interaction with the
    mediator complex, and sTF induced changes in the local chromatin structure
  - This differs from transcription factors in [[id:83bde0b9-5fb5-4184-9436-ffc051892dff][§Prokaryotes]], which interact
    directly with the \alpha sub-unit of RNA polymerase
** Diagram
[[file:.orgimg/Overview/2020-11-22_15-30-33_screenshot.png]]
* Yeast GAL Switch
  - Galactose is converted into glucose by a series of reaction that constitute
    the Leloir pathway
  - Galactose utilisation requires the Gal2 permease and 3 enzymes, Gal1, Gal7,
    and Gal10
  - Their expression is regulated in response to galactose by the sTF Gal4, the
    regulator protein Gal80, and the galactose sensor Gal3
** Mechanism
   1) Even when no galactose is present in the cell, Gal4 still binds the
      upstream activator region of the GAL [[id:6731cc14-75f6-4cfb-aa9a-f524260aca49][§Promoter]], and is linked to an
      activator domain that induces transcription
   2) In the absence of galactose, however, the Gal80 regulator protein binds
      this activator domain and actually represses transcription
   3) When galactose does enter the cell, it activates Gal3, which goes on to
      bind and titrate Gal80
   4) With all of the Gal80 bound to Gal3, the activator domain is free to
      promote transcription of the GAL genes
* Modular Structure
  - Most transcription factors have functionally separable DNA-binding and
    transcription activating domains
  - Gal4 consists of a singular DNA-binding domain connected to a singular
    activation domain via a flexible linker protein
** Diagram
[[file:.orgimg/Modular_Structure/2020-11-22_16-40-01_screenshot.png]]
* Yeast Two-Hybrid Assays
  - Used to analyse protein-protein interactions
  - The Gal4 linker region is replaced by a bait fusion on the DNA-binding
    domain and a prey fusion on the activator domain
  - Under normal circumstances, the cut Gal4 is inactive, and won't express a
    reporter gene, but if the bait and prey domains (the proteins of interest)
    interact, then they will combine to reform a functional Gal4
** Reporter Genes
*** LacZ
    - Produces the bacterial \beta-galactosidase
    - When produced, it can cleave X-gal to generate blue colonies
*** ADE2
    - Allows for the synthesis of adenine in deficient mutants
    - ade2 mutants develop a red pigmentation, this colouring is absent where
      the ADE2 gene is active
*** HIS3
    - Complements and auxotrophic allele, so anything that grows (on a minimal
      media) expresses HIS3
    - Increasing concentrations of the inhibitor 3-aminotriazole (3-AT) allow
      for the selection of clones that express higher levels of His3
*** Diagram
[[file:.orgimg/Yeast_Two-Hybrid_Assays/2020-11-22_16-56-11_screenshot.png]]
** Diagram
[[file:.orgimg/Yeast_Two-Hybrid_Assays/2020-11-22_16-47-21_screenshot.png]]
* Combinatoral Control
  - Transcription factors in Eukaryotes typically function as homo- or
    heterodimers
  - Some heterodimers recognise the same DNA sequence – target genes are
    responsive to distinct combination of activation domains under different
    conditions
  - Other heterodimers recognise different DNA sequences, increasing the number
    of potential targets
** Repressor / Activator Combinations
   - Transcriptional repressors also have modular domain structures
   - Repressor complexes are found associated with some actively transcribed
     genes, as well as silenced loci
   - Repressor complexes at actively transcribed genes are thought to find tune
     gene expression levels. Gene expression output is obtained at a range of
     values, rather than a binary on / off switch
*** Diagram
[[file:.orgimg/Combinatoral_Control/2020-11-22_17-09-24_screenshot.png]]
** Diagram
[[file:.orgimg/Combinatoral_Control/2020-11-22_17-06-56_screenshot.png]]
