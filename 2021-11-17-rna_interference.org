:PROPERTIES:
:ID:       dd99bded-4a40-4acf-91a8-c2db440902e8
:END:
#+TITLE: RNA Interference
#+DATE: 2021-11-17 (08:59)
#+ROAM_REF: [[~/Documents/University/Y4S1/The World of RNA/RNA Interference.pdf]]

Tags: [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][University]] [[id:5b180b65-94fa-4b69-a424-8b82b6a8d18b][The World of RNA]] [[id:a33e8933-6246-4c37-9201-3a4f33e894c6][RNA]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][Biology]] [[id:1d262d6a-6d81-432c-99fe-85823f1110cd][Gene Regulation]]

* Overview
** Terms
   - RNAi :: RNA interference
   - dsRNA :: double stranded RNA, duplex longer than 30nt
   - miRNA :: microRNA, 21-25nt
     - Encoded by endogenous genes
     - Hairpin ssRNA precursors – 70nt
     - Recognize multiple targets
   - siRNA :: short-interfering RNA, 21-25nt
     - Mostly exogenous origin
     - dsRNA precursors
     - May be target specific
* An Unexpected Result (Discovery)
  - Attempting to over-express a pigment producer, chalcone synthase, actually
    lead to the repression of both the introduced gene and the natural version
  - This co-suppression / quelling was a post-translational form of gene
    silencing – later discovered to be a form of RNA interference
  - As it turns out, it's the result of their inserted construct producing
    double stranded RNA (it brings one promoter with the insert, and being
    inserted at the right site could mean a second promoter transcribing an
    antisense strand as well)
  - More information here:
    https://academic.oup.com/bioscience/article/49/6/432/229392
** dsRNA as a Silencing Mechanism
   - Both sense and antisense templates produced RNA sufficient for silencing
   - Silencing can persist, even though RNA is easily degraded
   - RNA exhibiting silencing was often produced using bacteriophage RNA
     polymerases
   - These polymerases were specific, but could also make *ectopic transcripts*
     (produced when the polymerase turns around at the end of a piece of DNA and
     transcribes the anti-sense RNA strand as well as the original sense strand)
   - These ectopic transcripts led to the formation of some dsRNA in these
     preparations – as a result of base-pairing between the sense and anti-sense
     transcripts
** Proof of RNAi
   - RNA was designed to target the Unc-22 genes, leading to a "null phenotype"
     of uncoordinated twitching upon silencing 
   - Injected sense, anti-sense, or both into C. elegans gut
   - dsRNA was orders of magnitude more effective than ssRNA (effective even in
     amounts as little as a few molecules per cell)
   - Null phenotype also seen in progeny of injected worms, implying that this
     RNAi can carry on even into offspring
   - Inactivation was due to the degradation of the target RNA
   - dsRNA triggers this RNA interference (Fire et al. 1998)
   - dsRNA to GFP efficiently silenced expression
*** Diagram
[[file:.orgimg/An_Unexpected_Result_(Discovery)/2022-01-08_10-48-39_screenshot.png]]
** siRNA Causes This Co-Suppression
   - 25bp species of dsRNA were found in plants exhibiting co-suppression
     - These short dsRNA species were not found in "normal" plants free from
       co-suppression effects
     - These small dsRNAs are complementary to the mRNA they silence
   - In Drosophila, injected long dsRNA is processed into these shorter 21-25bp
     fragments
     - These fragments are siRNA
     - These siRNAs are necessary for the degradation of the target
   - [[http://web.mit.edu/beh.109/www/Module3/handouts/Handout%20Lect%201%20RNAi%20-%20www.ambion.com-techlib-hottopics-rnai.htm][A useful MIT resource]]
* miRNA Processing
  - Pri-miRNA looks like mRNA, but contains a hairpin in the middle
  - When this pri-miRNA is cut and the hairpin extracted, the hairpin structure
    is referred to as the pre-miRNA
  - Intonic pre-miRNA hairpins can also come from spliced out introns directly
** Cleavage of Hairpin
   - The Drosha + DGCRG complex is often referred to as the microprocessor
     (haha, very funny)
   - Drosha is RNase III type enzyme
   - Cleavage of the pri-mRNA results in 3' overhangs on the pre-miRNA
   - Cleavage normally occurs ~11bp up from the base of the stem
   - DGCR8 binds this stranded RNA at a hairpin motif and guides Drosha to the
     correct site for cleavage 
*** Diagram
[[file:.orgimg/miRNA_Processing/2022-01-08_12-13-11_screenshot.png]]
** miRNA Nuclear Export
   - Exportin 5 takes Ran-GTP (of which there is an excess in the nucleus) and
     transports the RNA out of the nucleus
   - In the cytoplasm, Ran-GTP is converted to Ran-GDP and the RNA is released
     to be processed by Dicer and used by by Argonaut to form the RISC
   - Ran-GDP is then re-imported into the nucleus and recycled
   - See [[id:e37352fb-cd19-47cf-b60b-1e4ad73f1e14][Nuclear Transport]] for more about the Ran-GTP export cycle
*** Diagram
[[file:.orgimg/miRNA_Processing/2022-01-08_12-21-55_screenshot.png]]
** Dicer Maturation
   - Dicer cuts the hair-pin pre-miRNAs to form an RNA duplex with 3' overhangs
   - Dicer can then phosphorylate 5' ends of dsRNA where an OH is present
*** Diagram
[[file:.orgimg/miRNA_Processing/2022-01-08_12-33-24_screenshot.png]]
** Argonaute
   - AGO can identify the strand that has it's 5' end at the more unstable side
     of the duplex (weaker base pairing) and eventually cleaves and discards the
     other "passenger" strand
   - Exactly how the identification of the "less stable" side of the duplex is
     done is still up for debate, but it seems that Dicer and another protein
     (R2D2 in Drosophila) can sense thermodynamic stability of the ends and AGO
     can also directly sense 5' nucleotides, preferring A or U over G or C
   - If the base pairing within the duplex isn't perfect (and it often isn't, in
     the case of miRNA), then Argonaute might not actually cleave the passenger
     strand
   - Unwinding (by another protein) keeps the guide strand of the miRNA
     (complementary to the mRNA), but gets rid of the potentially cleaved
     passenger strand, forming an RISC complex for RNA interference
*** Diagram
[[file:.orgimg/miRNA_Processing/2022-01-09_09-34-46_screenshot.png]]
* Mechanisms of RNAi Action
  - Target part of the RNA is almost always the 3' UTR of the mRNA
  - Endolytic Cleavage :: cleavage and degradation of the mRNA is the most
    common mechanism of action
  - Deadenylation :: CCR4-NOT is recruited to degrade the poly-A tail of an mRNA
    – this then leads to decapping and degradation
  - Proteolysis :: the RISC complex recruits enzymes to degrade the nascent
    peptide produced during translation
  - Initiation block :: represses ribosomal cap recognition and / or stops the
    60S subunit from joining
  - Elongation block :: slows elongation or causes ribosome "drop-off"
** Mammalian Resistance
   - An anti-viral pathway means that long dsRNA eventually leads to the
     activation of RNase L (which triggers global RNA degradation within the
     cell)
   - Also drives PKR, phosphorylating EIF2\alpha and creating a global block in
     translation initiation
   - Finally, long dsRNA can also trigger IFN-\alpha and IFN-\beta production,
     kick-starting a broader, multicellular immune response
   - Short RNA, however, doesn't seem to trigger this same destructive immune
     response and can knock down specific genes and leave others unaffected
   - These short, synthetic RNAi tools are called siRNA
*** Diagrams
**** Anti-Viral Response
[[file:.orgimg/Mechanisms_of_RNAi_Action/2022-01-09_10-42-23_screenshot.png]]
**** siRNA Effect
     - As an aside, use loading controls to prove that you've actually loading
       something into the lanes of your gel
[[file:.orgimg/Mechanisms_of_RNAi_Action/2022-01-09_10-47-59_screenshot.png]]
** Diagram
[[file:.orgimg/Mechanisms_of_RNAi_Action/2022-01-09_10-38-54_screenshot.png]]
** Limitations, Adaptations, and Applications
   - Not a knockout, more of a knock-down. Some protein is still produced, but it
     is significantly less
   - 90-95% knockdown is generally the limit
   - Silencing only lasts for ~4 cells divisions, as the siRNA is diluted and
     slowly degraded (maybe 6-9 hours in common tissue cultures)
*** Alternatives to siRNA
    - Could use plasmids that can be stably introduced into a chromosome that
      mimics the structure of Pri-miRNA
    - You can also build a library with promoters on either end of the target
      sequence so that dsRNA is produced post-transcription
    - This allows for the persistent production of RNAi molecules
    - This miRNA transcript can be inserted under an inducible promoter
    - Plasmids can now be ordered with a hairpin for nearly every human gene
**** Diagram
[[file:.orgimg/Mechanisms_of_RNAi_Action/2022-01-09_10-53-23_screenshot.png]]
*** Testing RNAi
    - GFP-connected to a pest-sequence makes it unstable and rapidly targeted to
      the proteosome
    - If you can target a protein of the proteosome with a short hairpin RNA, you
      can stop its activity and the GFP is no-longer degraded
    - This showed which subunits of the proteosome were essential for its
      function
**** Diagram
[[file:.orgimg/Mechanisms_of_RNAi_Action/2022-01-09_11-32-56_screenshot.png]]
*** RNAi By Feeding in Agar Plates
    - C. elegans likes to eat E. coli and can process long dsRNA
    - E. coli are grown to produce long dsRNA, the worms eat the E. coli, then
      the E. coli cells burst open and release the long dsRNA into the worm
    - This triggers the silencing of the gene(s) targeted by this long dsRNA
    - PCRed set of gene targets cloned into a plasmid with two promoters for both
      sense and anti-sense transcription
    - Result was a library of thousands of bacterial strains for knocking down
      nearly every gene in /C. elegans/
**** Diagram
[[file:.orgimg/Mechanisms_of_RNAi_Action/2022-01-09_11-35-29_screenshot.png]]

