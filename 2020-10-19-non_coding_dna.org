:PROPERTIES:
:ID:       7aebf77d-3915-4cca-a5cc-79ec9356b02d
:END:
#+TITLE: Non-Coding DNA
#+DATE: 2020-10-19 (11:59)
#+ROAM_REF: [[~/Documents/University/Y3S1/Genes, Genomes, and Chromosomes/MBB267 genome architecture overview.pdf]]
#+ROAM_REF: [[~/Documents/University/Y3S1/Genes, Genomes, and Chromosomes/Barnes genome architecture slides.pdf]]

Tags: [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:46a40352-2f1a-413b-871a-5ca06ce59a6e][§Genes, Genomes, and Chromosomes]] [[id:737fd2ce-19a8-4548-b4b9-11cbd49a9417][§DNA]]

* The C-Value Paradox
  - The C-value of an organism (the amount of nuclear DNA in a haploid cell) is
    a common measure of "genome size"
  - The "paradox" is that this size doesn't seem to correspond much to the
    complexity of an organism and that even quite similar organisms can have
    drastically different C-values
** Diagram
[[file:.orgimg/The_C-Value_Paradox/2020-10-19_23-43-36_screenshot.png]]
* Gene Density
  - Even though the human genome is far from the biggest one around, it's gene
    density is still quite low
  - Budding yeast carry around 549 genes per Mb, but humans carry only 6
  - Mammalian (and particularly some human genes) contain large amounts of
    intronic sequence. These sometimes allow for alternative splicing –
    producing slightly different proteins from the same gene in different
    tissues
  - There are, however, many other non-intronic, non-coding regions of the
    genome
** Diagram of Non-Coding DNA
[[file:.orgimg/Gene_Density/2020-10-20_13-30-26_screenshot.png]]
