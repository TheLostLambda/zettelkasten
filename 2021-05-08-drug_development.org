:PROPERTIES:
:ID:       0cf9c871-beae-4167-823a-0282f825c8fd
:END:
#+TITLE: Drug Development
#+DATE: 2021-05-08 (16:03)
#+ROAM_REF: [[~/Documents/University/Y3S2/Biochemistry/Lectures/lec1-Drugs and Chirality-annotated.pdf]]
#+ROAM_REF: [[~/Documents/University/Y3S2/Biochemistry/Lectures/lec2-Drug Binding to Target-annotated.pdf]]
#+ROAM_REF: [[~/Documents/University/Y3S2/Biochemistry/Lectures/lec3-adme1-annotated.pdf]]
#+ROAM_REF: [[~/Documents/University/Y3S2/Biochemistry/Lectures/lec4-adme2-annotated.pdf]]
#+ROAM_REF: [[~/Documents/University/Y3S2/Biochemistry/Lectures/lec5-fluoridation-annotated.pdf]]

Tags: [[id:33ee1804-bcfb-40c5-8f2c-62d54324c757][§Biochemistry 2]] [[id:8bf55d05-c4e8-4047-aa25-ba415a8e5f2d][§University]] [[id:5893fbe6-f459-4f04-8e6b-0c7935f6baca][§Scratch]] [[id:01031cee-99ae-4483-9b25-cd2dc9ffcca4][§Biology]]

* Overview
  - Drug development can follow 2 routes: either a compound is discovered to
    have a physiological effect before its molecular target is known, or a
    molecular target is identified first and a compound is designed specifically
    to bind it
  - Drug candidates often contain chiral centres ([[id:92b1cff7-6a19-4526-89ed-b621dde70ac9][§Chirality]]) whose orientations
    are vital to the binding of the compound
** Diagram
[[file:.orgimg/Overview/2021-05-08_18-26-03_screenshot.png]]
* Drug Binding
** Interactions
   - Drugs can bind their target in a number of (usually, but not always)
     non-covalent ways
*** \pi-\pi Stacking Interactions
    - Aromatic rings are subject to \pi-\pi stacking as delocalised ring electrons
      can interact with aromatic [[id:4e7d04c6-5af1-44fa-8bd7-95ea419f7390][§Amino Acids]] like Phe and Tyr
*** Salt-Bridging
    - Charged groups are often subject to ionic interactions and hydrogen bonding
    - Tertiary amines, for example, can interact with carboxyl groups like those
      in the side-chain of Asp or Glu
*** Hydrogen Bonding
    - Lone pairs act as acceptors and polar H-X bonds act as donors
    - Interactions can be formed with the side chains of amino acids like Ser or
      even with main-chain N-H / C=O groups
*** Hydrophobic Interactions
    - Hydrophobic groups like CH_3 can interact with any of the hydrophobic amino
      acids
    - These are stuck together by either water cages (as in [[id:5368fb48-8542-43e5-93fa-1e989ea10e28][§Protein Folding]]) or
      [[id:cca48346-bc82-47f1-9fc5-69ae9ff765ee][§Van Der Waals]] forces
*** Diagram
 [[file:.orgimg/Drug_Binding/2021-05-08_18-46-34_screenshot.png]]
** Target Parameters
*** Targets
    - Enzymes, receptor proteins, and channels / transporters make up the
      majority of drug targets
**** Diagram
[[file:.orgimg/Drug_Binding/2021-05-11_15-54-38_screenshot.png]]
*** K_D (Drug Ligand & Receptor)
    - Dissociation constant between the drug and its target – the lower the K_D,
      the more tighter the binding
    - When the drug concentration matches the K_D, 50% of the receptor is bound
    - See [[id:40afa1ec-8f37-4248-9c3c-2bc817e2ee2c][§Reaction Kinetics]] for more information
**** Diagram
*** EC_50 (Drug Potency / Response)
    - A value similar to the K_D, but instead of representing half-binding, EC_50
      is the drug concentration that exhibits 50% of the desired physiological
      effect
    - The Y-axis could be measured in % of bacteria killed, or % reduction in
      tumour size, or something similar
**** Diagram
[[file:.orgimg/Drug_Binding/2021-05-11_16-52-24_screenshot.png]]
*** IC_50 (Drug Inhibition of an Enzyme)
    - When the desired effect of a drug is the inhibition of an enzyme, the EC_50
      becomes an IC_50, representing the drug concentration that achieves 50%
      enzyme inhibition
**** Equation
     - $IC_{50} = K_i \left(\dfrac{1 + [S]}{K_M}\right)$
     - K_i is the K_D of the inhibitor and its target
     - [S] is the substrate concentration
     - K_M is the Michaelis-Menten constant
**** Diagram
[[file:.orgimg/Drug_Binding/2021-05-11_16-56-12_screenshot.png]]
*** Selectivity
    - The selectivity of a drug can be quantified as the ratio of its K_D with
      off-target molecules over its K_D with the target molecule
    - The higher this ratio, the more the drug is specific for a single target
      and the fewer non-target proteins it will bind
    - Generally, the higher the selectivity the better
**** Equation
     - $\text{Selectivity} = \dfrac{K_D\;\text{(Off-Target)}}{K_D\;\text{(Target)}}$
* Aspirin As An Example
  - Aspirin acetylates a crucial serine in the cyclooxygenase component of
    prostaglandin H_2 synthase
  - The released salicylic acid goes on to non-covalently bind the hydrophobic
    tunnel as well, obstructing the arachidonic acid substrate from reaching the
    active site
** Acetylation Mechanism Diagram
[[file:.orgimg/Aspirin_As_An_Example/2021-05-08_18-52-54_screenshot.png]]
** Blocking Structure Diagram
[[file:.orgimg/Aspirin_As_An_Example/2021-05-08_18-57-40_screenshot.png]]
* ADME(T) Parameters
  - Describe the fate of drugs that enter the body and whether or not they reach
    their target
  - Many drug candidates fail due to adverse ADME
** Absorption
   - A good rule-of-thumb is Lipinski's Rule of 5, which predicts poor
     absorption when it's rules are broken
   - Lipinski's rule only applies to drugs taken orally and absorbed in the
     intestine; biologics can bypass the MW limit by being intravenously
     administered
   - Note that lone pairs in delocalised \pi systems can not be hydrogen bond
     acceptors
   - When counting H-bond donors and acceptors, count the atoms involved, not
     the potential number of bonds
*** Lipinski's Rule of 5
    - Molecular Weight > 500 Da
    - Number of H-bond donors > 5
    - Number of H-bond acceptors > 10
    - Partition Coefficient, log(P) > 5
*** Partition Coefficient
    - Represents the tendency for a molecule to dissolve in membranes
    - Correlates with its ability to dissolve in organic solvents
    - Is calculated by dissolving ligand in a 1-octanol and water mixture. As
      the phases separate, a sample is taken from both the 1-octanol and water,
      then the ratio of ligand concentration in either phase is used to
      calculate log(P)
    - $\log{(P)} = \log_{10}{\left(\dfrac{[\text{Ligand (1-octanol)}]}{[\text{Ligand (aq)}]}\right)}$
** Distribution
   - Drug distribution is determined by a number of equilibria between bound and
     unbound forms throughout the compartments of the body
   - Many hydrophobic molecules that aren't soluble in the plasma are
     transported by human serum albumin
   - The distribution of drugs can be tracked using radioactive marker molecules
     and PET imaging
*** Diagrams
[[file:.orgimg/ADME_Parameters/2021-05-11_20-23-46_screenshot.png]]
[[file:.orgimg/ADME_Parameters/2021-05-11_20-24-20_screenshot.png]]
** Metabolism
   - The ultimate end of metabolism is to generate a more soluble form of the
     compound for excretion
   - Occurs in two steps: Oxidation and Conjugation
*** Oxidation
    - The most common form of oxidation is R-H -> R-OH
    - Ibuprofen undergoes this method of oxidation in the liver, incorporating a
      molecular oxygen via a reaction catalysed by cytochrome P450
    - The hydroxylation [[id:9d3748f3-0a85-4866-92ca-d14ce187f3a8][§Redox Reaction]] is complex and multi-step, involving the
      electrons from an NADPH being transferred by a haem group iron within the
      cytochrome P450
**** Diagrams
[[file:.orgimg/ADME_Parameters/2021-05-11_21-49-26_screenshot.png]]
[[file:.orgimg/ADME_Parameters/2021-05-11_21-54-44_screenshot.png]]
[[file:.orgimg/ADME_Parameters/2021-05-11_21-54-56_screenshot.png]]
*** Conjugation
    - Once a functional group has been introduced to the drug via oxidation, it
      can be coupled to another moiety
    - This coupling usually serves to increase the solubility of the drug,
      making it easier to excrete
    - This coupling can also have an affect on the activity of the drug; usually
      it leads to a reduction in the drug's activity, but in some cases (like
      Minoxidil sulfate) it's the coupled form that's biologically active
**** Diagrams
[[file:.orgimg/ADME_Parameters/2021-05-11_22-01-53_screenshot.png]]
** Excretion
   - Can proceed via two different pathways: kidney filtering, and enterohepatic
     cycling
   - Kidney filtering removes compounds < 60kDa, selectively reabsorbing solutes
     like glucose, nucleotides, and water, excreting the rest via the urine
   - In enterohepatic cycling, compounds not removed by the kidney are excreted
     from the liver in the bile. This, however, makes it possible for the drug
     to be reabsorbed in the small-intestine, *extending the half-life of the
     drug*
   - Excretion kinetics are often complex, but many exhibit some form of
     exponential decay
   - A useful parameter for drugs that follow this pattern is the half-life, the
     time it takes for half of the drug to be excreted
*** Diagram
[[file:.orgimg/ADME(T)_Parameters/2021-05-11_22-13-06_screenshot.png]]
** Toxicity
   - Modulate target too effectively (e.g. an anticoagulant being too effective
     could lead to unwanted haemorrhaging)
   - Off-target effects within the same target family (e.g. targeting one kinase
     might also have an affect on others)
   - Off-target effects in a different family (e.g. the K^{+} channel hERG in the
     heart is effected by a wide variety of drugs)
   - Toxic metabolic byproducts can also be an issue (e.g. paracetamol can form
     N-Acetyl-p-benzoquinone imine which is quite toxic to the liver)
*** Therapeutic Index
    - $\dfrac{LD_{50}}{EC_{50}}$
    - LD_50 is the lethal dose – the amount required to kill half of the
      individuals in a population
    - EC_50 is the point of half-effect
    - The higher the index, the better
*** Diagram
[[file:.orgimg/ADME(T)_Parameters/2021-05-11_22-27-26_screenshot.png]]
** Diagram
[[file:.orgimg/ADME_Parameters/2021-05-11_19-56-59_screenshot.png]]
* Improving ADME(T) With Fluoridation
  - To improve the ADME(T) parameters of a drug, C-H moieties are often replaced
    with C-F moieties
  - Fluorine is a good mimic of hydrogen with a similar [[id:cca48346-bc82-47f1-9fc5-69ae9ff765ee][§Van Der Waals]] radius (F
    = 1.45Å vs H = 1.1Å) and similar bond-length (C-F = 1.35Å vs C-H 1.1Å)
  - This substitution can: alter lipophilicity (usually an increase), increase
    the target affinity, improve metabolic stability, and overall increase
    bioavailability
  - The addition of fluorine is a delicate balance, if a little drops the pKa to
    help an uncharged form through the membrane, too much fluorine can impair
    the effect of the drug (particularly if the charged form is biologically
    active)
** Affect on pKa
   - The electronegativity of fluorine can tug on the electrons of neighbouring
     groups, stabilising their negative charge and making them more prone to
     lose protons and become acidic
*** Diagrams
[[file:.orgimg/Improving_ADME(T)_With_Fluoridation/2021-05-11_22-45-02_screenshot.png]]
[[file:.orgimg/Improving_ADME(T)_With_Fluoridation/2021-05-11_22-45-15_screenshot.png]]
** (Usually) Improved Lipophilicity
   - On average, adding a fluorine increases the lipophilicity of a drug,
     helping a higher proportion to pass through membranes and reach their
     target
   - This is likely because carbo-fluorine isn't very polarisable and the
     addition of a fluorine can sometimes shift the pKa of drugs enough that
     they become uncharged
   - Adding a fluorine can make some molecules with an oxygen nearby *more*
     polar and lipophobic by polarising the oxygen with the electronegative
     fluorine
   - This increased hydrophobicity can also improve its binding to human serum
     albumin, improving its transport around the body
*** Diagram
[[file:.orgimg/Improving_ADME(T)_With_Fluoridation/2021-05-11_22-51-51_screenshot.png]]
** Effect on Metabolism
   - The C-F bond is significantly stronger than the C-H bond
   - This stronger bond is harder for cytochrome P450 to oxidise than C-H,
     leading to a greatly decreased rate of metabolism
   - A reduction in metabolism leads to an increase in bioavailability 
** Example Diagrams
[[file:.orgimg/Improving_ADME(T)_With_Fluoridation/2021-05-11_23-01-59_screenshot.png]]
[[file:.orgimg/Improving_ADME(T)_With_Fluoridation/2021-05-11_23-02-06_screenshot.png]]
[[file:.orgimg/Improving_ADME(T)_With_Fluoridation/2021-05-11_23-02-26_screenshot.png]]
* Clinical Trials
** 1) Preclinical Drug Discovery
   - This is where candidates are screened and animal testing is performed
** 2) Phase I
   - This phase is small and tests the drug for safety in young, healthy adults
** 3) Phase II
   - A somewhat larger phase in which the drug is tested on both healthy and
     unhealthy individuals to establish a measure of efficacy
   - This phase is also used to work out an optimal dosage and further assess
     safety
** 4) Phase III
   - Further safety and efficacy testing on an even larger population with a
     fixed dosage
   - The participants are monitored for adverse side-effects
** 5) Phase IV
   - This is broad clinical use, an eye is kept out for low-frequency
     side-effects
* Takeaways
  - Drug candidates must be potent modulators of their targets
  - Drugs have to compete with concentration of the natural substrate
  - Drugs must have suitable properties to reach their targets (ADME)
  - Toxicity can limit drug effectiveness
  - Drugs can be discovered by serendipity, screening, or design
  - Three dimensional structure of the target can aid in drug design

